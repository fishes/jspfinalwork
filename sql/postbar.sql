/*
Navicat MySQL Data Transfer

Source Server         : dev@localhost
Source Server Version : 50626
Source Host           : localhost:3306
Source Database       : postbar

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2015-12-19 18:24:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for classtable
-- ----------------------------
DROP TABLE IF EXISTS `classtable`;
CREATE TABLE `classtable` (
  `ClassID` int(11) NOT NULL AUTO_INCREMENT COMMENT '类ID',
  `ClassName` varchar(255) NOT NULL COMMENT '类名字',
  `Type` int(11) NOT NULL COMMENT '类别（1为大类，0为小类）',
  `Belong` int(11) NOT NULL COMMENT '归属（只有当Type为0时）',
  `TopicNumber` int(11) DEFAULT '0' COMMENT '主题数量',
  `reserved1` varchar(255) DEFAULT NULL,
  `reserved2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ClassID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of classtable
-- ----------------------------
INSERT INTO `classtable` VALUES ('1', 'Java', '1', '0', '0', null, null);
INSERT INTO `classtable` VALUES ('2', 'c/c++', '1', '0', '0', null, null);
INSERT INTO `classtable` VALUES ('3', 'python', '1', '0', '0', null, null);
INSERT INTO `classtable` VALUES ('4', 'php', '1', '0', '0', null, null);
INSERT INTO `classtable` VALUES ('5', 'Java A', '0', '1', '0', null, null);
INSERT INTO `classtable` VALUES ('6', 'Java B', '0', '1', '0', null, null);
INSERT INTO `classtable` VALUES ('7', 'Java C', '0', '1', '0', null, null);
INSERT INTO `classtable` VALUES ('8', 'Java D', '0', '1', '0', null, null);
INSERT INTO `classtable` VALUES ('9', 'c/c++ A', '0', '2', '0', null, null);
INSERT INTO `classtable` VALUES ('10', 'c/c++ B', '0', '2', '0', null, null);
INSERT INTO `classtable` VALUES ('11', 'c/c++ C', '0', '2', '0', null, null);
INSERT INTO `classtable` VALUES ('12', 'c/c++ D', '0', '2', '0', null, null);
INSERT INTO `classtable` VALUES ('13', 'python A', '0', '3', '0', null, null);
INSERT INTO `classtable` VALUES ('14', 'python B', '0', '3', '0', null, null);
INSERT INTO `classtable` VALUES ('15', 'python C', '0', '3', '0', null, null);
INSERT INTO `classtable` VALUES ('16', 'python D', '0', '3', '0', null, null);
INSERT INTO `classtable` VALUES ('17', 'php A', '0', '4', '0', null, null);
INSERT INTO `classtable` VALUES ('18', 'php B', '0', '4', '0', null, null);
INSERT INTO `classtable` VALUES ('19', 'php C', '0', '4', '0', null, null);
INSERT INTO `classtable` VALUES ('20', 'php D', '0', '4', '0', null, null);

-- ----------------------------
-- Table structure for collectiontable
-- ----------------------------
DROP TABLE IF EXISTS `collectiontable`;
CREATE TABLE `collectiontable` (
  `CollectionID` int(11) NOT NULL AUTO_INCREMENT COMMENT '收藏ID',
  `UserID` int(11) NOT NULL COMMENT '用户ID',
  `TopicID` int(11) DEFAULT '0' COMMENT '主题ID',
  `ClassID` int(11) DEFAULT '0' COMMENT '小类ID',
  `reserved1` varchar(255) DEFAULT NULL,
  `reserved2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CollectionID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of collectiontable
-- ----------------------------
INSERT INTO `collectiontable` VALUES ('1', '1', '0', '5', null, null);
INSERT INTO `collectiontable` VALUES ('2', '1', '0', '6', null, null);

-- ----------------------------
-- Table structure for floortable
-- ----------------------------
DROP TABLE IF EXISTS `floortable`;
CREATE TABLE `floortable` (
  `FloorID` int(11) NOT NULL AUTO_INCREMENT COMMENT '楼层ID',
  `Content` longtext NOT NULL COMMENT '跟帖内容',
  `TopicID` int(11) NOT NULL COMMENT '主题ID',
  `FloorRegistTime` timestamp NOT NULL COMMENT '时间',
  `UserID` int(11) NOT NULL COMMENT '用户ID',
  `QuoteFloorID` int(11) DEFAULT NULL COMMENT '引用楼层',
  `reserved1` varchar(255) DEFAULT NULL,
  `reserved2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`FloorID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of floortable
-- ----------------------------
INSERT INTO `floortable` VALUES ('1', '第一个帖子', '1', '2015-12-08 21:54:26', '1', null, null, null);
INSERT INTO `floortable` VALUES ('2', '第二个帖子', '1', '2015-12-31 22:07:09', '2', '1', null, null);

-- ----------------------------
-- Table structure for topictable
-- ----------------------------
DROP TABLE IF EXISTS `topictable`;
CREATE TABLE `topictable` (
  `TopicID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主题ID',
  `TopicName` varchar(255) NOT NULL COMMENT '主题名字',
  `TopicRegistTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '时间',
  `ClassID` int(11) NOT NULL COMMENT '小类ID',
  `UserID` int(11) NOT NULL COMMENT '用户ID',
  `RecentTime` date NOT NULL COMMENT '最近回复时间',
  `RecentName` varchar(255) NOT NULL COMMENT '最近回复人',
  `RecentUserID` int(11) NOT NULL COMMENT '最后回复用户id',
  `FloorsCount` int(11) DEFAULT '1',
  `reserved3` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`TopicID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of topictable
-- ----------------------------
INSERT INTO `topictable` VALUES ('1', 'aaa', '2015-12-19 20:47:24', '5', '1', '2015-12-29', 'cccNickName', '1', '2', null);
INSERT INTO `topictable` VALUES ('2', 'bbb', '2015-12-19 20:58:14', '6', '1', '2015-12-29', 'cccNickName', '1', '1', null);
INSERT INTO `topictable` VALUES ('3', 'ccc', '2015-12-19 20:58:16', '7', '1', '2015-12-29', 'cccNickName', '1', '1', null);
INSERT INTO `topictable` VALUES ('4', 'ddd', '2015-12-19 20:58:17', '9', '1', '2015-12-29', 'cccNickName', '1', '1', null);
INSERT INTO `topictable` VALUES ('6', 'fff', '2015-12-19 20:58:23', '19', '1', '2015-12-29', 'cccNickName', '1', '1', null);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `UserName` varchar(255) NOT NULL COMMENT '用户姓名',
  `NickName` varchar(255) NOT NULL COMMENT '用户昵称',
  `PassWord` varchar(255) NOT NULL COMMENT '密码',
  `RegistTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '时间',
  `Email` varchar(255) DEFAULT '' COMMENT '邮箱',
  `Level` varchar(255) DEFAULT NULL COMMENT '权限控制',
  `TopicNum` int(11) NOT NULL DEFAULT '0' COMMENT '主题总数',
  `reserved2` varchar(255) DEFAULT NULL,
  `reserved3` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'aaa', 'aaaNickname', 'aaaPass', '2015-12-19 20:18:04', 'aaa@aaa.com', '', '2', null, null);
INSERT INTO `user` VALUES ('2', 'bbb', 'bbbNickname', 'bbbPass', '2015-12-19 20:18:08', null, '1', '5', null, null);