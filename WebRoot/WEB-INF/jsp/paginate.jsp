<%@page import="java.net.URLDecoder"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<style>
.x-pagination {
	display: inline-block;
	padding-left: 0;
	margin: 20px 0;
	border-radius: 4px;
}

.x-pagination>li {
	display: inline;
}

.x-pagination>li>a, .x-pagination>li>span {
	position: relative;
	float: left;
	padding: 6px 12px;
	line-height: 1.428571429;
	text-decoration: none;
	color: #337ab7;
	background-color: #ffffff;
	border: 1px solid #dddddd;
	margin-left: -1px;
}

.x-pagination>li:first-child>a, .x-pagination>li:first-child>span {
	margin-left: 0;
	border-bottom-left-radius: 4px;
	border-top-left-radius: 4px;
}

.x-pagination>li:last-child>a, .x-pagination>li:last-child>span {
	border-bottom-right-radius: 4px;
	border-top-right-radius: 4px;
}

.x-pagination>li>a:hover, .x-pagination>li>span:hover, .x-pagination>li>a:focus,
	.x-pagination>li>span:focus {
	z-index: 3;
	color: #23527c;
	background-color: #eeeeee;
	border-color: #dddddd;
}

.x-pagination>.active>a, .x-pagination>.active>span, .x-pagination>.active>a:hover,
	.x-pagination>.active>span:hover, .x-pagination>.active>a:focus,
	.x-pagination>.active>span:focus {
	z-index: 2;
	color: #ffffff;
	background-color: #337ab7;
	border-color: #337ab7;
	cursor: default;
}

.x-pagination>.disabled>span, .x-pagination>.disabled>span:hover,
	.x-pagination>.disabled>span:focus, .x-pagination>.disabled>a,
	.x-pagination>.disabled>a:hover, .x-pagination>.disabled>a:focus {
	color: #777777;
	background-color: #ffffff;
	border-color: #dddddd;
	cursor: not-allowed;
}
</style>
<link rel="stylesheet" href="<%=basePath%>framework/bootstrap/css/bootstrap.min.css" />
<!-- Forgive 后面一部分东西还没能够抠出来  -->
<div id="x-pagination-component">
	<ul class="x-pagination">
		<%
			long currentPage = (Long) request.getAttribute("currentPage");
			long totalPages = (Long) request.getAttribute("totalPages");
			String linkPath = (String) request.getAttribute("linkPath");
			String targetPost = (String) request.getAttribute("targetPost");
			if (linkPath.contains("?"))
				linkPath += "&currentPage=";
			else
				linkPath += "?currentPage=";

			//显示上一页
			if (currentPage > 1)
				out.println("<li><a href=\"" + basePath + linkPath
						+ (currentPage - 1) + "\">&laquo;</a></li>");
			else
				out.println("<li class=\"disabled\"><a href=\"javascript:;\">&laquo;</a></li>");

			//===========================================================================================
			//
			if (totalPages <= 5)
				for (int i = 1; i <= totalPages; i++)
					out.println("<li class=\""
							+ (i == currentPage ? "active" : "")
							+ "\"><a href=\"" + basePath + linkPath + i
							+ "\">" + i + "</a>");
			else if (currentPage < 5)
				for (int i = 1; i <= 5; i++)
					out.println("<li class=\""
							+ (i == currentPage ? "active" : "")
							+ "\"><a href=\"" + basePath + linkPath + i
							+ "\">" + i + "</a>");
			else if (currentPage == totalPages)
				for (int i = -4; i <= 0; i++)
					out.println("<li class=\"" + (i == 0 ? "active" : "")
							+ "\"><a href=\"" + basePath + linkPath
							+ (currentPage + i) + "\">" + (currentPage + i)
							+ "</a>");
			else
				for (int i = -3; i <= 1; i++)
					out.println("<li class=\"" + (i == 0 ? "active" : "")
							+ "\"><a href=\"" + basePath + linkPath
							+ (currentPage + i) + "\">" + (currentPage + i)
							+ "</a>");

			//===========================================================================================
			//显示下一页
			if (currentPage < totalPages)
				out.print("<li><a href=\"" + basePath + linkPath
						+ (currentPage + 1) + "\">&raquo;</a></li>");
			else
				out.println("<li class=\"disabled\"><a href=\"javascript:;\">&raquo;</a></li>");
		%>
		<li class="disabled"><a href="javascript:;">Current/All:<%=currentPage + "/" + totalPages%></a></li>
		<form action="<%=basePath + targetPost%>" name="pagination_form"
			class="form-inline" style="float:left;" method="post">
			<div class="form-group">
				<label for="number_to_go_input">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Page
					To Go</label> <input type="text" name="currentPage" id="number_to_go_input"
					class="form-control" />
			</div>
			<input type="button" onClick="goToSepecificPage();" value="Go"
				class="btn btn-primary" />
		<%
			String[] extraParams = (String[]) request.getAttribute("extraParams");
			if (extraParams != null)
				for(int index = 0; index < extraParams.length; index++)
				{
					int position = extraParams[index].indexOf("=");
					out.println("<input type=\"hidden\" name=\"" + 
								extraParams[index].substring(0, position) +
								"\" value=\"" + 
								URLDecoder.decode(extraParams[index].substring(position+1), "UTF-8") + "\" />");
				}
		%>
		</form>
	</ul>
	<script type="text/javascript">
		function goToSepecificPage() {
			var value = document.getElementById("number_to_go_input").value;
			if (!/^[1-9]+$/.test(value)) {
				alert("The Page Number Is Invalid !");
				return false;
			} else {
				if (value > <%=totalPages%>)
					document.getElementById("number_to_go_input").value = <%=totalPages%>;
				document.pagination_form.submit();
				return true;
			}
		}
	</script>
</div>