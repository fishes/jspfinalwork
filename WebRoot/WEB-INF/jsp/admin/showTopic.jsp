<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>主题</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

<!-- 美化表格的时候插入的 -->	
<link
	href="<%=basePath %>framework/bootstrap/css/bootstrap.min.css"
	rel="stylesheet"> 
	
  </head>
  
  <body>
    ${username}
   <form action="<%=basePath%>adminTopic/showBaseInfo" method="post">
   		请选择你想要操作的类：<select name="class">
   		            <option value="all" ${topicSelected == "all" ? "selected" : ""}>所有类</option>
   					<option value="Java" ${topicSelected == "Java" ? "selected" : ""}>Java</option>
   						<c:forEach var="javaClassResult" items="${javaClassResults}">
							<option value="${javaClassResult.ClassName}" ${topicSelected == javaClassResult.ClassName ? "selected" : ""}>
							&nbsp;&nbsp;&nbsp;&nbsp;${javaClassResult.ClassName}</option>
			    		</c:forEach>
			    	<option value="c/c++" ${topicSelected == "c/c++" ? "selected" : ""}>c/c++</option>
			    		<c:forEach var="cClassResult" items="${cClassResults}">
							<option value="${cClassResult.ClassName}" ${topicSelected == cClassResult.ClassName ? "selected" : ""}>
							&nbsp;&nbsp;&nbsp;&nbsp;${cClassResult.ClassName}</option>
			    		</c:forEach>
			    	<option value="python" ${topicSelected == "python" ? "selected" : ""}>python</option>
			    		<c:forEach var="pythonClassResult" items="${pythonClassResults}">
							<option value="${pythonClassResult.ClassName}" ${topicSelected == pythonClassResult.ClassName ? "selected" : ""}>
							&nbsp;&nbsp;&nbsp;&nbsp;${pythonClassResult.ClassName}</option>
			    		</c:forEach>
			    	<option value="php" ${topicSelected == "php" ? "selected" : ""}>php</option>
			    		<c:forEach var="phpClassResult" items="${phpClassResults}">
							<option value="${phpClassResult.ClassName}" ${topicSelected == phpClassResult.ClassName ? "selected" : ""}>
							&nbsp;&nbsp;&nbsp;&nbsp;${phpClassResult.ClassName}</option>
			    		</c:forEach>	
   			</select>
   			<input type="submit" value="搜索"/><br/>
   </form>
   
   <!-- 表单 -->   
   <table class="table table-striped table-hover">
   	<thead>主题信息</thead>
	<tbody>
   		<tr>
   			<td>主题名称</td>
   			<td>主题发布时间</td>
   			<td>进入该主题</td>
   			<td>删除该主题</td>
   		</tr>
   		<c:forEach var="result" items="${results}">
			<tr>
				<td>${result.TopicName}</td>
				<td>${result.TopicRegistTime}</td>
				<td><form action="<%=basePath %>adminFloor/showBaseInfo" method="post">
					<input type="hidden" name="topicName" value="${result.TopicName}" />
					<input type="hidden" name="topicId" value="${result.TopicID}" />
					<input type="submit" value="进入该主题">
				</form></td>
				<td><form action="<%=basePath %>adminTopic/deleteInfo" method="post">
					<input type="hidden" name="class" value="${classes}" />
					<input type="hidden" name="topicId" value="${result.TopicID}" />
					<input type="submit" value="删除该主题">
				</form></td>
			</tr>
		</c:forEach>
   	</tbody>
   </table>

<center><%@ include file="../paginate.jsp" %></center> 
</body>
</html>
