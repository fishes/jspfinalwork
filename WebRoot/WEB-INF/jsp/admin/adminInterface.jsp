
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>管理员操作界面</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">   
	
<!-- 美化表格的时候插入的 -->	
	<script src="http://apps.bdimg.com/libs/jquery/2.0.0/jquery.min.js"></script>
<script
	src="http://apps.bdimg.com/libs/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<link
	href="http://apps.bdimg.com/libs/bootstrap/3.3.0/css/bootstrap.min.css"
	rel="stylesheet"> 
  </head>
  
  
  
  <body>
  <div class="text" style="text-align:center;"><h1>Welcome</h1></div>
  <br/>
  <div class="text" style=" text-align:center;">${username}</div>
  <br/>
  <br/>
   <center><form action="<%=basePath%>adminTopic/showBaseInfo" method="post">
   		请选择你想要操作的类：<br/> <br/><select name="class">
   		            <option value="all">所有类</option>
   					<option value="Java">Java</option>
   						<c:forEach var="javaClassResult" items="${javaClassResults}">
							<option value="${javaClassResult.ClassName}">&nbsp;&nbsp;&nbsp;&nbsp;${javaClassResult.ClassName}</option>
			    		</c:forEach>
			    	<option value="c/c++">c/c++</option>
			    		<c:forEach var="cClassResult" items="${cClassResults}">
							<option value="${cClassResult.ClassName}">&nbsp;&nbsp;&nbsp;&nbsp;${cClassResult.ClassName}</option>
			    		</c:forEach>
			    	<option value="python">python</option>
			    		<c:forEach var="pythonClassResult" items="${pythonClassResults}">
							<option value="${pythonClassResult.ClassName}">&nbsp;&nbsp;&nbsp;&nbsp;${pythonClassResult.ClassName}</option>
			    		</c:forEach>
			    	<option value="php">php</option>
			    		<c:forEach var="phpClassResult" items="${phpClassResults}">
							<option value="${phpClassResult.ClassName}">&nbsp;&nbsp;&nbsp;&nbsp;${phpClassResult.ClassName}</option>
			    		</c:forEach>	
   			</select> 
<!-- <input type="text" name="class"/> -->   
	
<!-- 
	     小类：<select name="smallclass">
   				<c:forEach var="smallClassResult" items="${smallClassResults}">
					<option value="${smallClassResult.ClassName}">${smallClassResult.ClassName}</option>
			    </c:forEach>
   			</select>
 -->   		
<!-- <input type="text" name="smallclass"/> -->
   		
   		<input type="submit" value="搜索"/><br/>
   </form></center>
   


<!-- 表单   
   <table class="table table-striped table-hover">
   	<thead>主题信息</thead>
	<tbody>
   		<tr>
   			<td>主题名称</td>
   			<td>主题发布时间</td>
   			<td>进入该主题</td>
   			<td>删除该主题</td>
   		</tr>
   	</tbody>
   </table>
-->      
  </body>
</html>
