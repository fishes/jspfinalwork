<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>楼层</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
<!-- 美化表格的时候插入的 -->	
<link
	href="<%=basePath %>framework/bootstrap/css/bootstrap.min.css"
	rel="stylesheet"> 
	
  </head>
  
  <body>
   <center><h1>${names} 楼层信息</h1></center> <br>
    <!-- 表单 -->   
   <table class="table table-striped table-hover">
   	<thead></thead>
	<tbody>
   		<tr>
            <!-- <td>楼层</td> -->   			
   			<td>内容</td>
   			<td>发布时间</td>
   			<td>删除该楼</td>
   		</tr>
   		<c:forEach var="result" items="${results}">
			<tr>
				<td>${result.Content}</td>
				<td>${result.FloorRegistTime}</td>
				<td><form action="" method="post">
					<input type="submit" value="删除该楼">
				</form></td>
			</tr>
		</c:forEach>
   	 </tbody>
   	</table>
   	
   	<center><%@ include file="../paginate.jsp" %></center> 
  </body>
</html>
