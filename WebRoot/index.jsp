<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>jspFinalWork index</title>
	
	<link rel="stylesheet" type="text/css" href="framework/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="resource/css/layout.css">
	<link href="editor/themes/default/css/umeditor.css" type="text/css" rel="stylesheet">
	<link href="resource/css/editor-layout.css" type="text/css" rel="stylesheet">
  </head>
  
  <body ng-app="homeApp">
	  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="errModalTitle" ng-controller="errCtrl">
		  <div class="modal-dialog modal-sm">
		    <div class="modal-content">
		     <div class="modal-header">
		     	<h3 class="modal-title" id="errModalTitle">错误信息</h3>
		     </div>
		     <div class="modal-body">
		     	{{err.msg}}
		     </div>
		     <div class="modal-footer">
		     	<button class="btn btn-default" type="button" data-dismiss="modal">close</button>
		     </div>
		    </div>
		  </div>
		</div>
  		<nav class="navbar navbar-color navbar-fixed-top">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="/">Brand</a>
		    </div>
		
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="navbar-collapse">
		    <form class="navbar-form navbar-left" role="form" style="height:35px">
		    	<div class="col-md-10">
			        <div class="input-group" style="height:30px;">
			          <input type="text" class="form-control" placeholder="Search" style="height:30px;">
			          <span class="input-group-btn">
	        	          <button class="btn btn-sm bit-info" type="button" style="height:30px;">
	            	         <span class="glyphicon glyphicon-search"></span>
	                	  </button>
	               	  </span>
		        	</div>
		    	</div>
		     </form>
		      <ul class="nav navbar-nav navbar-right">
		        <li class="dropdown">
		          <a  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">java<span class="caret"></span></a>
		          <ul class="dropdown-menu" role="menu">
		          <li><a href="#/list/5">java A</a></li>
		            <li><a href="#/list/6">java B</a></li>
		            <li><a href="#/list/7">java C</a></li>
		            <li><a href="#/list/8">java D</a></li>
		          </ul>
		        </li>
		        
		        <li class="dropdown">
		          <a  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">C/C++<span class="caret"></span></a>
		          <ul class="dropdown-menu" role="menu">
		            <li><a href="#/list/9">C/C++ A</a></li>
		            <li><a href="#/list/10">C/C++ B</a></li>
		            <li><a href="#/list/11">C/C++ C</a></li>
		            <li><a href="#/list/12">C/C++ D</a></li>
		          </ul>
		        </li>
		        <li class="dropdown">
		          <a  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Python<span class="caret"></span></a>
		          <ul class="dropdown-menu" role="menu">
		            <li><a href="#/list/13">Python A</a></li>
		            <li><a href="#/list/14">Python B</a></li>
		            <li><a href="#/list/15">Python C</a></li>
		            <li><a href="#/list/16">Python D</a></li>
		          </ul>
		        </li>
		        <li class="dropdown">
		          <a  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">php<span class="caret"></span></a>
		          <ul class="dropdown-menu" role="menu">
		            <li><a href="#/list/17">php A</a></li>
		            <li><a href="#/list/18">php B</a></li>
		            <li><a href="#/list/19">php C</a></li>
		            <li><a href="#/list/20">php D</a></li>
		          </ul>
		        </li>
		         <a href="#/login"><small>注册/登陆</small></a>
		      </ul>
		      
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		<div class="container-fuild">
		<!—- 左侧导航栏 -->
		  	<div class="col-md-2" id="user-panel" ng-controller="userCtrl">
			  	<div class="row">
			  		<sapn class="white-font">&nbsp Hi,{{user.nickName}}</sapn><br/>
			  	</div>
		  		<div class="row">
			  		  <!-- Nav tabs -->
					  <ul class="nav nav-tabs" role="tablist">
					    <li role="presentation" class="active"><a href="#my-topic" aria-controls="my-topic" role="tab" data-toggle="tab">我的帖子</a></li>
					  </ul>
					  <!-- Tab panes -->
					  <div class="tab-content">
					    <div role="tabpanel" class="tab-pane" id="my-topic">
					    	<ul>
					    		<li class="white-font" ng-repeat="topic in topics"><a href="#topic/{{topic.topicId}}">{{topic.title}}</a></li>
					    	</ul>
					    </div>
					  </div>
		  		</div>
		  	</div>
		  	<div class="col-md-10 col-md-offset-2" id="topic-panel">
		  		<div ng-view></div>
		  	</div>
		</div>
  	
  	
  	<script type="text/javascript" src="framework/jquery/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="framework/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="framework/angular/angular.min.js"></script>
	<script type="text/javascript" src="framework/angular/angular-route.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="editor/umeditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="editor/umeditor.min.js"></script>
    <script type="text/javascript" src="editor/lang/zh-cn/zh-cn.js"></script>
	<script type="text/javascript" src="resource/js/homeApp.js"></script>
	<script type="text/javascript" src="resource/js/controllers.js"></script>
	<script type="text/javascript" src="resource/js/services.js"></script> 
	
  </body>
</html>
