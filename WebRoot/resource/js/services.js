var services = angular.module("services",[]);
services.service("errService", function() {
	var self = this;
	self.errCode = 0;
	self.errMsg = " ";
	this.setErrCode = function(errCode){
		self.errCode = errCode;
	};
	this.setErrMsg = function(errMsg){
		self.errMsg = errMsg;
	};
})
//--------------------------------------------------
.service("AjaxService",["$http","errService",function($http,errService){
	var baseUrl = "http://localhost:8080/JspFinalWork/";
	this.doPost = function(url,data,callback){
		var fianlUrl = baseUrl + url;
		$http.post(fianlUrl , data).success(function(data) {
			if(!data.errCode||data.errCode == undefined){
				if(callback != undefined ||callback != null)
					callback(data.data);
			}else{
				errService.setErrCode(data.errCode);
				errService.setErrMsg(data.errMsg);
			}
		}).error(function(data) {
			errService.setErrCode(data.errCode);
			errService.setErrMsg(data.errMsg);
		});
	};
	this.doGet = function(url,params,callback){
		var fianlUrl = baseUrl + url;
		$http.get(fianlUrl,params).success(function(data) {
			if(!data.errCode||data.errCode == undefined){
				if(callback != undefined ||callback != null)
					callback(data.data);
			}else{
				errService.setErrCode(data.errCode);
				errService.setErrMsg(data.errMsg);
			}
		}).error(function(data) {
			errService.setErrCode(data.errCode);
			errService.setErrMsg(data.errMsg);
		});
	};
}])
//----------------------------------------------------------
/*homeApp.service("pageService", function() {
	var self = this;
	this.page ={};
	this.setPageSize = function(pageSize){
		self.page['pageSize'] = pageSize;
	}
	this.setPageNow = function(pageNow){
		self.page['pageNow'] = pageNow;
	}
})*/
//-----------------------------------------------------------
.service("userService", ["AjaxService",function(AjaxService) {
	var self = this;
	this.user={
		"userId" : "",
		"username" : "",
		"nickname" : "",
		"registDate" : "",
		"email" : "",
		"level" : "",
		"topics" : [],
		"classes" : [],
	};
	this.topics = [];
	//登陆成功后，后台不会返回用户信息
	this.login = function(userName,password,callback){
		var data = {"username" : userName,
					"password" : password
					};
		AjaxService.doPost("log/in/", data,callback);
	};
	this.getUserInfo = function(){
		AjaxService.doGet("user/info",null,function(backData){
			self.user = backData.data
		});
	};
	this.logout = function(){
		AjaxService.doPost("log/out", null, null);
		self.user = {};
	};
	this.register = function(data){
		AjaxService.doPost("user/register", data, null);
	}
}])
//----------------------------------------------------------------------
.service("topicService", ["AjaxService","$location","$http",function(AjaxService,$location,$http) {
	
	this.getList = function(pageSize,pageNow,classId,callback){
		AjaxService.doGet("topic/list",{params: {"countPerPage": pageSize ,"pageNum" : pageNow,"classId":classId} }, callback);
	};
	this.newTopic = function(topic){
		AjaxService.doPost("topic/new", topic, null);
	};
	this.getFloors = function(topicId,pageSize,pageNow,callback){
		AjaxService.doGet("floor/list", {params: {"countPerPage": pageSize ,"pageNum" : pageNow,"topicId":topicId}}, callback);
	};
	//获得帖子的详情页面中的标题
	this.getTopic = function(topicId,callback){
		AjaxService.doGet("topic/get", {params: {"topicId":topicId}}, callback);
	}
	
	this.newFloor = function(postDate){
		AjaxService.doPost("floor/new", postDate, null);
	}
}]);
