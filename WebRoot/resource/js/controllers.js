var controllers = angular.module("controllers", ["services"])
controllers.controller("topicListCtrl", ["$scope","topicService","$routeParams","userService",function($scope,topicService,$routeParams,userService) {
	var classId = $routeParams.classId;
	$scope.user = userService.user;
	$scope.pageNow = 1;
	$scope.setPageNow = function(page){
		if(page>0)
			$scope.pageNow=page;
	}
	var getList =function(){
		return topicService.getList(25,$scope.pageNow,classId,function(backData){
			$scope.topics =backData.result;
		});
	}
	$scope.$watch("pageNow",getList);
	
}]).controller("errCtrl",["errService","$scope",function($scope,errService){
	$scope.err = errService.err;
	
	
	
	
}]).controller("topicCtrl", ["$scope","topicService","$routeParams","$location","$anchorScroll",function($scope,topicService,$routeParams,$location,$anchorScroll) {
	//实例化编辑器
	var um = UM.getEditor('myEditor');
	var topicId = $routeParams.topicId;
	$scope.pageNow = 1;
	$scope.setPageNow = function(page){
		if(page>0)
			$scope.pageNow=page;
	}
//	var getTopic = function(){
//		return 
		topicService.getTopic(topicId,function(backData){
			$scope.topic=backData;
		});
//	}
	var getFloors = function(){
		return topicService.getFloors(topicId, 25, $scope.pageNow, function(backData) {
			$scope.floors=backData.result;
		});
	}
	$scope.toResponse = function(floor){
		if(floor != undefined || floor != null){
			var text = floor.content;
			if(text)
			$scope.toResponseText ="<p class='quote-text'>"+ floor.content +"</p>";
			$location.hash("editor");
            $anchorScroll();
		}
	}
	$scope.submitResponse = function(topicId){
		var content= um.getContent();
		var postDate ={content:content,topicId:topicId};
		topicService.newTopic(postDate);
	}
	$scope.$watch("pageNow",getFloors);
	
	
}]).controller("userCtrl", ["$scope","userService",function($scope,userService){
	$scope.user = userService.user;
	$scope.topics = userService.topics;
}]).controller("registerCtrl", ["$scope","userService",function($scope,userService){
	var user = {username:"",
				nickname:"",
				password:"",
				confirmPassword:"",
				email:""};
	$scope.user=user;
	
	$scope.register= function(){
		if(user.username != null && user.nickname != null && user.password != null && user.confirmPassword != null && user.email != null){
			if(user.password == user.confirmPassword){
				userService.register(user);
			}
		}
	}
}]).controller("loginCtrl", ["$scope","userService",function($scope,userService){
	var user = {username:"",
				password:""};
	$scope.user=user;
	
	$scope.login= function(){
		if(user.username != null && user.password != null){
			var fail = -1;
			userService.login(user.username, user.password,function(backData){
				fail= backData.data;
			});
			if(!fail){
				userService.getUserInfo();
			}
		}
	}
}])

