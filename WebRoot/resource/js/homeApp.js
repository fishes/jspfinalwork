var homeApp = angular.module("homeApp", ["ngRoute","controllers","services"]);

homeApp.config(["$routeProvider",function($routeProvider) {
	var tempBaseUrl = "/JspFinalWork/resource/template/";
	$routeProvider.when("/",{
			redirectTo : "/list/5"
		}).when("/list/:classId", {
			controller: "topicListCtrl",
			templateUrl: tempBaseUrl+"topicList.html"
		}).when("/topic/:topicId",{
			controller: "topicCtrl",
			templateUrl: tempBaseUrl+'topic.html'
			//resolve: {"getTopicIndex":["topicService","$route",function(topicService,$route){
			//	var topicId = $route.current.params["topicId"];
			//	topicService.getTopicIndex(topicId, 25, 1);
			//}]}
		}).when("/login",{
			controller: "loginCtrl",
			templateUrl: tempBaseUrl+'login.html'
		}).when("/register",{
			controller: "registerCtrl",
			templateUrl: tempBaseUrl+'register.html'
		}).when("/publish",{
			controller: "publishCtrl",
			templateUrl: tempBaseUrl+'postopic.html'
		}).otherwise({
			redirectTo: "/"
		});
}]);
/*.run(['$rootScope', '$window', '$location', '$log', function ($rootScope, $window, $location, $log) {  
    var locationChangeStartOff = $rootScope.$on('$locationChangeStart', locationChangeStart);  
    var locationChangeSuccessOff = $rootScope.$on('$locationChangeSuccess', locationChangeSuccess);  
  
    var routeChangeStartOff = $rootScope.$on('$routeChangeStart', routeChangeStart);  
    var routeChangeSuccessOff = $rootScope.$on('$routeChangeSuccess', routeChangeSuccess);  
  
    function locationChangeStart(event) {  
        $log.log('locationChangeStart');  
        $log.log(arguments);  
    }  
  
    function locationChangeSuccess(event) {  
        $log.log('locationChangeSuccess');  
        $log.log(arguments);  
    }  
  
    function routeChangeStart(event) {  
        $log.log('routeChangeStart');  
        $log.log(arguments);  
    }  
  
    function routeChangeSuccess(event) {  
        $log.log('routeChangeSuccess');  
        $log.log(arguments);  
    }  
}])*/

//--------------------------------------------------
