package jsp.finalwork.any.servlet;

import javax.servlet.annotation.WebServlet;

import tk.lx.servlet.BaseHttpServlet;

@WebServlet(urlPatterns="/adminlogin/*")
public class AdminLogin extends BaseHttpServlet
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void index()
	{
		renderPage("admin/adminLogin.jsp");

	}
}
