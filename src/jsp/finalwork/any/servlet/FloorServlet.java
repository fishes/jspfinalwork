package jsp.finalwork.any.servlet;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;

import jsp.finalwork.any.service.FloorService;
import tk.lx.dao.Page;
import tk.lx.servlet.BaseHttpServlet;
import tk.lx.util.ValidateUtil;

@WebServlet(urlPatterns="/adminFloor/*")
public class FloorServlet extends BaseHttpServlet
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//show所有楼层
	public void showBaseInfo() throws IOException
	{
		FloorService floorService=new FloorService();
		String i=getParam("topicId");
		String topicName=getParam("topicName");
		String currentPage = getParam("currentPage");
		long cp = Long.valueOf(!ValidateUtil.validateString(currentPage) ? "1" : currentPage);
		//ArrayList<HashMap<String, Object>> floorBase=floorService.getBaseInfo();
		
		Page results=floorService.setPage(cp, 1L, "select * from floortable where TopicID="+i+" order by FloorRegistTime desc");
		
		setPaginateInfo("adminFloor/showBaseInfo", results);
		setAttr("results", results.getResult());
		setAttr("names", topicName);
		
		renderPage("/WEB-INF/jsp/admin/showFloor.jsp");
	}
}
