package jsp.finalwork.any.servlet;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.annotation.WebServlet;

import jsp.finalwork.any.service.ClassService;
import jsp.finalwork.any.service.UserService;
import tk.lx.servlet.BaseHttpServlet;

@WebServlet(urlPatterns="/admin/*")
public class UserServlet extends BaseHttpServlet
{
	private static final long serialVersionUID = 1L;
	
/**
 * 	public void getBaseInfo()
	{
		UserService userService=new UserService();
		ArrayList<HashMap<String, Object>> result=userService.getBaseInfo();
		setAttr("result", result);
		renderPage("any/adminLogin.jsp");
	}
 * @throws IOException 
	
 */

	public void checkAdmin() throws IOException
	{
		UserService userService=new UserService();
		String username=(String) getParam("username");
		String password=(String) getParam("password");
		setAttr("username", username);
		
		//TopicService topicService=new TopicService();
		ClassService classService=new ClassService();
		//ArrayList<HashMap<String, Object>> classResults=classService.getClassBaseInfo();
		ArrayList<HashMap<String, Object>> javaClassResults=classService.getBlongClassBaseInfo(1);
		//setAttr("classResults",classResults);
		setAttr("javaClassResults",javaClassResults);
		ArrayList<HashMap<String, Object>> cClassResults=classService.getBlongClassBaseInfo(2);
		setAttr("cClassResults",cClassResults);
		ArrayList<HashMap<String, Object>> phpClassResults=classService.getBlongClassBaseInfo(4);
		setAttr("phpClassResults",phpClassResults);
		ArrayList<HashMap<String, Object>> pythonClassResults=classService.getBlongClassBaseInfo(3);
		setAttr("pythonClassResults",pythonClassResults);
		
		boolean is= userService.checkUser(username, password);
		if(is==true){
			renderPage("/WEB-INF/jsp/admin/adminInterface.jsp");
		}else{
			renderPage("/WEB-INF/jsp/admin/adminLogin.jsp");
		}
	}
	
}
