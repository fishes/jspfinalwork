package jsp.finalwork.any.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.annotation.WebServlet;

import jsp.finalwork.any.service.ClassService;
import jsp.finalwork.any.service.TopicService;
import tk.lx.dao.Page;
import tk.lx.servlet.BaseHttpServlet;
import tk.lx.util.ValidateUtil;

@WebServlet(urlPatterns="/adminTopic/*")
public class TopicServlet extends BaseHttpServlet
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void showBaseInfo() throws IOException
	{
		TopicService topicService=new TopicService();
		ClassService classService=new ClassService();
		
		String currentPage = getParam("currentPage");
		long cp = Long.valueOf(!ValidateUtil.validateString(currentPage) ? "1" : currentPage);


		ArrayList<HashMap<String, Object>> javaClassResults=classService.getBlongClassBaseInfo(1);
		setAttr("javaClassResults",javaClassResults);
		ArrayList<HashMap<String, Object>> cClassResults=classService.getBlongClassBaseInfo(2);
		setAttr("cClassResults",cClassResults);
		ArrayList<HashMap<String, Object>> phpClassResults=classService.getBlongClassBaseInfo(4);
		setAttr("phpClassResults",phpClassResults);
		ArrayList<HashMap<String, Object>> pythonClassResults=classService.getBlongClassBaseInfo(3);
		setAttr("pythonClassResults",pythonClassResults);
		
		String topic=getParam("class");
		setAttr("classes", topic);
		if("all".equals(topic))
		{
			Page results=topicService.setPage(cp, 1L, "select * from topictable order by TopicRegistTime desc");
			//ArrayList<HashMap<String, Object>> results=topicService.getBaseInfo();
			setPaginateInfo("adminTopic/showBaseInfo?class=all", results);
			setAttr("results", results.getResult());
		}else if("Java".equals(topic)){
			Page results=topicService.setPage(cp, 1L, "select * from topictable where ClassId=5 or ClassId=6 or ClassId=7 or ClassId=8 order by TopicRegistTime desc");
			//ArrayList<HashMap<String, Object>> results=topicService.getClassBaseInfo(5, 6, 7, 8);
			setPaginateInfo("adminTopic/showBaseInfo?class=Java", results);
			setAttr("results", results.getResult());
		}else if("c/c++".equals(topic)){
			Page results=topicService.setPage(cp, 1L, "select * from topictable where ClassId=9 or ClassId=10 or ClassId=11 or ClassId=12 order by TopicRegistTime desc");
			//ArrayList<HashMap<String, Object>> results=topicService.getClassBaseInfo(9, 10, 11, 12);
			setPaginateInfo("adminTopic/showBaseInfo?class=c/c++", results);
			setAttr("results", results.getResult());
		}else if("python".equals(topic)){
			Page results=topicService.setPage(cp, 1L, "select * from topictable where ClassId=13 or ClassId=14 or ClassId=15 or ClassId=16 order by TopicRegistTime desc");
			//ArrayList<HashMap<String, Object>> results=topicService.getClassBaseInfo(13, 14, 15, 16);
			setPaginateInfo("adminTopic/showBaseInfo?class=python", results);
			setAttr("results", results.getResult());
		}else if("php".equals(topic)){
			Page results=topicService.setPage(cp, 1L, "select * from topictable where ClassId=17 or ClassId=18 or ClassId=19 or ClassId=20 order by TopicRegistTime desc");
			//ArrayList<HashMap<String, Object>> results=topicService.getClassBaseInfo(17, 18, 19, 20);
			setPaginateInfo("adminTopic/showBaseInfo?class=php", results);
			setAttr("results", results.getResult());
		}else{
			int i=classService.selectId(topic);
			Page results=topicService.setPage(cp, 1L, "select * from topictable where ClassId="+i+" order by TopicRegistTime desc");
			//ArrayList<HashMap<String, Object>> results=topicService.getSmallClassBaseInfo(i);
			setPaginateInfo("adminTopic/showBaseInfo?class="+topic, results);
			setAttr("results", results.getResult());
		}
		
		setAttr("topicSelected", topic);
		renderPage("/WEB-INF/jsp/admin/showTopic.jsp");
	}
	
	public void deleteInfo() throws IOException
	{
		TopicService topService=new TopicService();
		String topicId=getParam("topicId");
		topService.deleteInfo(topicId);
		
		showBaseInfo();
	}

}
