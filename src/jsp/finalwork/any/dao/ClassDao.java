package jsp.finalwork.any.dao;

import java.util.ArrayList;
import java.util.HashMap;

import tk.lx.dao.BaseDao;

public class ClassDao extends BaseDao
{

	//查找所有大类
	public ArrayList<HashMap<String, Object>> selectClassBaseInfo()
	{
		return selectList("select * from classtable where Type=1");
	}
	//查找所有小类
	public ArrayList<HashMap<String, Object>> selectSmallClassBaseInfo()
	{
		return selectList("select * from classtable where Type=0");
	}
	//查找属于某一大类的小类
	public ArrayList<HashMap<String, Object>> selectBlongClassBaseInfo(int i)
	{
		return selectList("select * from classtable where Type=0 and Belong=?", i);
	}
	
	//查找某一类的ID
	public int selectId(String str)
	{
		HashMap<String, Object> results= selectOne("select ClassID from classtable where ClassName=?", str);
		return (int) results.get("ClassID");
	}
	//删除某条信息
}
