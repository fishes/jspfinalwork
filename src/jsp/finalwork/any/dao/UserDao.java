package jsp.finalwork.any.dao;

import java.util.ArrayList;
import java.util.HashMap;

import tk.lx.dao.BaseDao;
import tk.lx.dao.mapping.FirstLowerMapping;

/**
 * 用户表Dao
 * @author xyj
 *
 */
public class UserDao extends BaseDao
{
	//查找用户信息
	public ArrayList<HashMap<String, Object>> selectBaseInfo()
	{
		return selectList("select * from user","");	
	}
	//删除用户
	public void deleteBaseInfo(int userId)
	{
		delete("delete from user where UserID=?", userId);
	}
	
	@Override
	protected void configResultMapping()
	{
		setResultMapping(new FirstLowerMapping());
	}
}
