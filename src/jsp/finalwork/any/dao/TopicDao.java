package jsp.finalwork.any.dao;

import java.util.ArrayList;
import java.util.HashMap;

import tk.lx.dao.BaseDao;
import tk.lx.dao.mapping.FirstLowerMapping;

public class TopicDao extends BaseDao
{
	//查找所有主题
	public ArrayList<HashMap<String, Object>> selectBaseInfo()
	{
		//long currentPage = 1;
		//return paginate(currentPage, "select* from topictable");
		return selectList("select* from topictable");
	}
	//删除主题
	public void deleteBaseInfo(String topicId)
	{
		delete("delete from topictable where TopicID=?", topicId);
	}
	//查找属于某一大类的所有主题
	public ArrayList<HashMap<String, Object>> selectClassBaseInfo(int i,int j,int k,int l)
	{
		return selectList("select * from topictable where ClassId=? or ClassId=? or ClassId=? or ClassId=?", i,j,k,l);
	}
	
	//查找属于某一小类的所有主题
	public ArrayList<HashMap<String, Object>> selectSmallClassBaseInfo(int i)
	{
		
		return selectList("select * from topictable where ClassId=?", i);
	}
	
	
	@Override
	protected void configResultMapping()
	{
		setResultMapping(new FirstLowerMapping());
	}
}
