package jsp.finalwork.any.dao;

import java.util.ArrayList;
import java.util.HashMap;

import tk.lx.dao.BaseDao;
import tk.lx.dao.mapping.FirstLowerMapping;

public class FloorDao extends BaseDao
{
	//查楼层信息
	public ArrayList<HashMap<String, Object>> selectBaseInfo()
	{
		return selectList("select*from floortable");
	}
	//删除楼层信息
	public void deleteBaseInfo(int floorId)
	{
		delete("delete from floortable where FloorID=?", floorId);
	}
	
	@Override
	protected void configResultMapping()
	{
		setResultMapping(new FirstLowerMapping());
	}
}
