package jsp.finalwork.any.service;

import java.util.ArrayList;
import java.util.HashMap;

import jsp.finalwork.any.dao.UserDao;
import tk.lx.annotation.SingleDao;
import tk.lx.service.BaseService;
import tk.lx.util.ValidateUtil;

public class UserService extends BaseService
{
	@SingleDao
	private UserDao userDao;
	
	
	//得到用户表信息
	public ArrayList<HashMap<String, Object>> getBaseInfo()
	{
		return userDao.selectBaseInfo();
	}
	//删除用户表末一条信息
	public void deleteBaseInfo(int userId)
	{
		userDao.deleteBaseInfo(userId);
	}
	//管理员登入验证
	public boolean checkUser(String username, String password)
	{

		if (ValidateUtil.validateString(username)
			&& ValidateUtil.validateString(password))
		{
			if (userDao.selectOne("select * from user " 
								+ "where UserName=? and PassWord=? and Level=1", 
								username, password) == null)
				return false;
			else
				return true;
		}
			
		return false;
		
	}
}
