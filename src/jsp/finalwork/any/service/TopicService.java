package jsp.finalwork.any.service;

import java.util.ArrayList;
import java.util.HashMap;

import jsp.finalwork.any.dao.TopicDao;
import tk.lx.annotation.SingleDao;
import tk.lx.dao.Page;
import tk.lx.service.BaseService;

public class TopicService extends BaseService
{
	@SingleDao
	private TopicDao topicDao;
	//获得所有的主题
	public ArrayList<HashMap<String, Object>> getBaseInfo()
	{
		return topicDao.selectBaseInfo();
	}
	
	//获得某一大类的所有主题
	public ArrayList<HashMap<String, Object>> getClassBaseInfo(int i,int j,int k,int l)
	{
		return topicDao.selectClassBaseInfo(i,j,k,l);
	}
	//获得某一小类的所有主题
	public ArrayList<HashMap<String, Object>> getSmallClassBaseInfo(int i)
	{
		return topicDao.selectSmallClassBaseInfo(i);
	}
	//删除某个主题
	public void deleteInfo(String topicId)
	{
		topicDao.deleteBaseInfo(topicId);
	}
	//设置分页
	public Page setPage(long currentPage, long recordPerPage, String sql, Object ... args)
	{
		return topicDao.paginate(currentPage, recordPerPage, sql, args);
	}
	
}
