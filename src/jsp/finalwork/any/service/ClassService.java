package jsp.finalwork.any.service;

import java.util.ArrayList;
import java.util.HashMap;

import jsp.finalwork.any.dao.ClassDao;
import tk.lx.annotation.SingleDao;
import tk.lx.service.BaseService;

public class ClassService extends BaseService
{
	@SingleDao
	private ClassDao classDao;

	//得到所有大类信息
	public ArrayList<HashMap<String, Object>> getClassBaseInfo()
	{
		return classDao.selectClassBaseInfo();
	}
	//得到所有小类信息
	public ArrayList<HashMap<String, Object>> getSmallClassBaseInfo()
	{
		return classDao.selectSmallClassBaseInfo();
	}
	//得到属于某一大类的小类
	public ArrayList<HashMap<String, Object>> getBlongClassBaseInfo(int i)
	{
		return classDao.selectBlongClassBaseInfo(i);
	}
	
	//查找某一类的ID
	public int selectId(String str)
	{
		return classDao.selectId(str);
	}
}
