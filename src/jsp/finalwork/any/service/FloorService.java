package jsp.finalwork.any.service;


import java.util.ArrayList;
import java.util.HashMap;

import jsp.finalwork.any.dao.FloorDao;
import tk.lx.annotation.SingleDao;
import tk.lx.dao.Page;
import tk.lx.service.BaseService;

public class FloorService extends BaseService{

	@SingleDao
	private FloorDao floorDao;
	
	//获得某一主题的所有楼层
	public ArrayList<HashMap<String, Object>> getBaseInfo()
	{
		return floorDao.selectBaseInfo();
	}
	
	//设置分页
		public Page setPage(long currentPage, long recordPerPage, String sql, Object ... args)
		{
			return floorDao.paginate(currentPage, recordPerPage, sql, args);
		}
}
