/**
 * @FileName LoginServlet.java
 * @Package jsp.finalwork.lx.servlet
 * @author muxue
 * @date Dec 2, 2015
 */
package jsp.finalwork.lx.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import jsp.finalwork.lx.bean.UserBean;
import jsp.finalwork.lx.checker.LoginChecker;
import jsp.finalwork.lx.service.UserService;
import tk.lx.annotation.ActionMapping;
import tk.lx.annotation.PermissionCheck;
import tk.lx.annotation.RequestParam;
import tk.lx.annotation.SingleService;
import tk.lx.servlet.BaseHttpServlet;

/**
 * @ClassName: LoginServlet
 * @Description 用户登录处理
 * @author muxue
 * @date Dec 2, 2015
 */
@WebServlet(urlPatterns="/log/*")
public final class LoginServlet extends BaseHttpServlet
{
	private static final long serialVersionUID = 1L;

	@SingleService
	UserService userService;
	
	/**
	 * 验证用户账号密码，已测试
	 * errCode:
	 * 0：操作成功
	 * -1：错误的用户名或密码
	 * -2：已经登录
	 * @param username 用户账号
	 * @param password用户密码
	 */
	@ActionMapping("in")
	public void userLogin(@RequestParam("username") String username,
							@RequestParam("password") String password)
							throws IOException, ServletException
	{
		if (getSessionAttr("userId") != null) 
		{
			renderJSONResult(-2, "Have login before ...", null);
			return;
		}
		
		if (userService.checkUser(username, password))
		{
			UserBean user = userService.getUserPutInSession(username);
			if (user == null)
			{
				renderJSONResult(-2, "Something error while retrive user info ...", null);
				return;
			}
			
			setSessionAttr("userId", user.getUserID());
			setSessionAttr("userLevel", user.getLevel());
			setSessionAttr("userName", user.getUserName());
			renderJSONResult(0, null);
		}
		else
			renderJSONResult(-1, "Invalid username or password", null);
	}
	
	
	/**
	 * 当前用户退出登录，已测试
	 * 退出后页面上什么响应，由前台决定
	 * errCode：
	 * 0：操作成功
	 */
	@ActionMapping("out")
	@PermissionCheck(checker=LoginChecker.class)
	public void userLogout() throws IOException, ServletException
	{
		getSession().invalidate();
		renderJSONResult(0, null);
	}
}
