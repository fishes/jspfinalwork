/**
 * @FileName FavorServlet.java
 * @Package jsp.finalwork.lx.servlet
 * @author muxue
 * @date Dec 30, 2015
 */
package jsp.finalwork.lx.servlet;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;

import jsp.finalwork.lx.checker.LoginChecker;
import jsp.finalwork.lx.service.UserService;
import tk.lx.annotation.ActionMapping;
import tk.lx.annotation.PermissionCheck;
import tk.lx.annotation.RequestParam;
import tk.lx.annotation.SingleService;
import tk.lx.dao.Page;
import tk.lx.servlet.BaseHttpServlet;

/**
 * @ClassName: FavorServlet
 * @Description 用户关注主题相关操作
 * @author muxue
 * @date Dec 30, 2015
 */
@WebServlet(urlPatterns="/favor/*")
public class FavorServlet extends BaseHttpServlet
{

	private static final long serialVersionUID = 1L;

	@SingleService
	private UserService userService;
	

	/**
	 * 用户登录之后可以获取到已经关注的主题列表
	 * errCode:
	 * 0：操作成功
	 * -1：数据库操作出错
	 * 
	 * @param pageNum 当前页码，必要参数
	 * @throws IOException
	 */
	@ActionMapping("list")
	@PermissionCheck(checker = LoginChecker.class)
	public void getUserFavors(@RequestParam("pageNum") long pageNum) throws IOException
	{
		int userId = (int) getSessionAttr("userId");
		Page result = userService.listFavors(userId, pageNum);
		
		if (result == null)
			renderJSONResult(-1, "something error while retriving data ...", null);
		else
			renderJSONResult(0, result);
	}

	
	/**
	 * 用户登录之后可以关注指定的主题
	 * errCode:
	 * 0：操作成功
	 * -1：数据库操作出错
	 * -2: 用户已经关注了指定主题，不能再次关注
	 * 
	 * @param topicId 指定的topic的id
	 * @throws IOException
	 */
	@ActionMapping("add")
	@PermissionCheck(checker = LoginChecker.class)
	public void addUserFavor(@RequestParam("topicId") int topicId) throws IOException
	{
		int userId = (int) getSessionAttr("userId");
		if (userService.isFavored(userId, topicId))
		{
			renderJSONResult(-2, "the user had add it to the favor list ...");
			return;
		}

		if (!userService.addFavor(userId, topicId))
			renderJSONResult(-1, "something error while retriving data ...");
		else
			renderJSONResult(0, null);
	}

	
	/**
	 * 用户登录之后可以删除指定已关注的主题
	 * errCode:
	 * 0：操作成功
	 * -1：数据库操作出错
	 * -2：用户没有关注该主题
	 * 
	 * @param topicId 指定的topic的id
	 * @throws IOException
	 */
	@ActionMapping("delete")
	@PermissionCheck(checker = LoginChecker.class)
	public void deleteUserFavor(@RequestParam("topicId") int topicId) throws IOException
	{
		int userId = (int) getSessionAttr("userId");
		if (!userService.isFavored(userId, topicId))
		{
			renderJSONResult(-2, "the user did not add the topic to the favor list ...");
			return;
		}

		if (!userService.deleteFavor(userId, topicId))
			renderJSONResult(-1, "something error while retriving data ...");
		else
			renderJSONResult(0, null);
	}

}
