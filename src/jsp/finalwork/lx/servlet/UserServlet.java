/**
 * @FileName UserServlet.java
 * @Package jsp.finalwork.lx.servlet
 * @author muxue
 * @date Dec 1, 2015
 */
package jsp.finalwork.lx.servlet;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;

import jsp.finalwork.lx.bean.UserBean;
import jsp.finalwork.lx.checker.LoginChecker;
import jsp.finalwork.lx.service.UserService;
import tk.lx.annotation.ActionMapping;
import tk.lx.annotation.PermissionCheck;
import tk.lx.annotation.RequestParam;
import tk.lx.annotation.SingleService;
import tk.lx.dao.Page;
import tk.lx.servlet.BaseHttpServlet;

/**
 * @ClassName: UserServlet
 * @Description 用户相关servlet 
 * @author muxue
 * @date Dec 1, 2015
 */
@WebServlet(urlPatterns="/user/*")
public class UserServlet extends BaseHttpServlet
{
	private static final long serialVersionUID = 1L;
	
	@SingleService
	UserService userService;
	
	/**
	 * 用户登录之后可以从这边获取用户发表过的帖子，已测试
	 * errCode:
	 * 0：操作成功
	 * -1：数据库操作出错
	 * 
	 * 返回数据示例：
	 * {"errCode":0,
	 * "errMsg":null,
	 * "data":[{"NickName":"aaaNickname","FloorID":1,"Content":"第一个帖子"}]}
	 * @param pageNum 当前页码，从1开始，是必要参数
	 * @throws IOException
	 */
	@ActionMapping("list/floor")
	@PermissionCheck(checker=LoginChecker.class)
	public void getUserPostFloors(@RequestParam("pageNum") long pageNum) throws IOException
	{
		int userId = (int) getSessionAttr("userId");
		Page result = userService.getPostFloors(userId, pageNum);
		
		if (result == null)
			renderJSONResult(-1, "something error while retriving user info ...", null);
		else
			renderJSONResult(0, result);
	}
	
	
	/**
	 * 用户登录之后可以从这边获取用户信息已测试
	 * errCode:
	 * 0：操作成功
	 * -1：数据库操作出错
	 * @throws IOException
	 */
	@ActionMapping("info")
	@PermissionCheck(checker=LoginChecker.class)
	public void getUserInfo() throws IOException
	{
		int userId = (int) getSessionAttr("userId");
		UserBean user = userService.getBasicUserInfo(userId);
		
		if (user == null)
			renderJSONResult(-1, "something error while retriving user info ...", null);
		else
			renderJSONResult(0, user);
	}
	
	// =====================================================================================
	// 用户注册相关
	// =====================================================================================
	
	/**
	 * 用户注册处理，未测试
	 * errCode：
	 * 0：操作成功
	 * 
	 * @throws IOException
	 */
	@ActionMapping("register")
	public void registerUser(@RequestParam("username") String username,
								@RequestParam("password") String password,
								@RequestParam("nickname") String nickname) throws IOException
	{
		String email = getParam("email");
		
		if (userService.isUsernameUsed(username)) 
		{
			renderJSONResult(-1, "the username is used ...");
			return;
		}
		
		userService.insertNewUser(username, password, nickname, email);
		renderJSONResult(0, null);
	}
}
