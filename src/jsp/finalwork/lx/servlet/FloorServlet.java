/**
 * @FileName FloorServlet.java
 * @Package jsp.finalwork.lx.servlet
 * @author muxue
 * @date Dec 1, 2015
 */
package jsp.finalwork.lx.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.annotation.WebServlet;

import jsp.finalwork.lx.checker.LoginChecker;
import jsp.finalwork.lx.service.FloorService;
import tk.lx.annotation.ActionMapping;
import tk.lx.annotation.PermissionCheck;
import tk.lx.annotation.RequestParam;
import tk.lx.annotation.SingleService;
import tk.lx.dao.Page;
import tk.lx.servlet.BaseHttpServlet;
import tk.lx.util.ValidateUtil;

/**
 * @ClassName: FloorServlet
 * @Description 帖子相关servlet
 * @author muxue
 * @date Dec 1, 2015
 */
@WebServlet(urlPatterns = "/floor/*")
public class FloorServlet extends BaseHttpServlet
{
	private static final long serialVersionUID = 1L;

	@SingleService
	FloorService floorService;

	/**
	 * 根据主题ID和当前分页页码获取帖子的数据，已测试
	 * errCode:
	 * 0：操作成功
	 * -1：数据库操作中出错
	 * 
	 * @param topicIdString
	 *            主题的ID，是必要参数
	 * @param pageNumString
	 *            当前主题的哪一个页，默认是第一页，一页20条记录，不是必要参数
	 * @throws IOException
	 */
	@ActionMapping("list")
	public void getFloorsInTopic(@RequestParam("topicId") int topicId) throws IOException
	{
		int pageNum = 1;
		String pageNumString = getParam("pageNum");
		if (ValidateUtil.validateInteger(pageNumString))
			pageNum = Integer.valueOf(pageNumString);

		Page page = floorService.getFloorsInPage(pageNum, topicId);

		if (page == null)
			renderJSONResult(-1, "something error while retriving page data ...", null);
		else
			renderJSONResult(0, copeFloorListResult(page));
	}
	
	// 转换名称以符合前台命名
	private Page copeFloorListResult(Page page)
	{
		ArrayList<HashMap<String, Object>> floors = page.getResult();
		ArrayList<HashMap<String, Object>> copedFloors = new ArrayList<>();
		
		for (HashMap<String,Object> floor : floors)
		{
			HashMap<String, Object> tempFloor = new HashMap<>();
			HashMap<String, Object> tempUser = new HashMap<>();
			tempFloor.put("content", floor.get("Content"));
			
			tempFloor.put("user", tempUser);
			tempUser.put("userId", floor.get("UserID"));
			tempUser.put("nickName", floor.get("NickName"));
			tempUser.put("registTime", floor.get("RegistTime"));
			tempUser.put("topicNum", floor.get("TopicNum"));
			
			copedFloors.add(tempFloor);
		}
		
		page.setResult(copedFloors);
		return page;
	}

	/**
	 * 插入一个新楼层，未测试
	 * 说明：贱贱当初数据库没有增加楼层的层数（编号），所以这里引用楼层的功能也不复存在，以后再说吧
	 * 
	 * --> 现在还是选择启用QuoteFloorID
	 * 
	 * errCode:
	 * 0：操作成功
	 * -1：数据库操作中出错
	 * 
	 * @param content
	 * @param topicId
	 * @throws IOException
	 */
	@ActionMapping("new")
	@PermissionCheck(checker = LoginChecker.class)
	public void insertNewFloor(@RequestParam("content") String content, 
								@RequestParam("topicId") int topicId) throws IOException
	{
		String userName = (String) getSessionAttr("userName");
		int userId = (int) getSessionAttr("userId");
		int quoteFloorId = -1;
		if (ValidateUtil.validateInteger(getParam("quoteId"))) 
			quoteFloorId = Integer.valueOf(getParam("quoteId"));
		
		if (floorService.addNewFloor(content, topicId, userId, userName, quoteFloorId))
			renderJSONResult(0, null);
		else
			renderJSONResult(-1, "Unknown Error while coping ...", null);
	}
}
