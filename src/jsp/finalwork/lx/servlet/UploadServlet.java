/**
 * @FileName UploadServlet.java
 * @Package jsp.finalwork.lx.servlet
 * @author muxue
 * @date Dec 6, 2015
 */
package jsp.finalwork.lx.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

import jsp.finalwork.lx.checker.LoginChecker;
import tk.lx.annotation.PermissionCheck;
import tk.lx.servlet.BaseHttpServlet;
import tk.lx.servlet.helper.UploadFileInfo;

/**
 * @ClassName: UploadServlet
 * @Description 上传文件操作
 * @author muxue
 * @date Dec 6, 2015
 */
@WebServlet("/upload")
@MultipartConfig
public class UploadServlet extends BaseHttpServlet
{
	private static final long serialVersionUID = 1L;
	
	
	@Override
	/**
	 * 只允许几种常见的图片格式
	 */
	protected boolean isFileTypeAllowed(String extendName)
	{
		if (".jpg".equalsIgnoreCase(extendName))
			return true;
		else if (".jpeg".equalsIgnoreCase(extendName))
			return true;
		else if (".gif".equalsIgnoreCase(extendName))
			return true;
		else if (".png".equalsIgnoreCase(extendName))
			return true;
		else if (".bmp".equalsIgnoreCase(extendName))
			return true;
		else
			return false;
	}


	/**
	 * 文件上传接口，已测试
	 */
	@PermissionCheck(checker=LoginChecker.class)
	public void index() throws IOException, ServletException
	{
		UploadFileInfo fileInfo = saveFile("upfile");
		if (fileInfo == null)
			renderUMError();
		else 
			renderUMSuccess(fileInfo);
	}
}
