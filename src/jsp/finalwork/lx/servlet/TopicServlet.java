/**
 * @FileName TopicServlet.java
 * @Package jsp.finalwork.lx.servlet
 * @author muxue
 * @date Nov 30, 2015
 */
package jsp.finalwork.lx.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.annotation.WebServlet;

import jsp.finalwork.lx.bean.TopicBean;
import jsp.finalwork.lx.checker.LoginChecker;
import jsp.finalwork.lx.service.TopicService;
import tk.lx.annotation.ActionMapping;
import tk.lx.annotation.PermissionCheck;
import tk.lx.annotation.RequestParam;
import tk.lx.annotation.SingleService;
import tk.lx.dao.Page;
import tk.lx.servlet.BaseHttpServlet;
import tk.lx.util.ValidateUtil;

/**
 * @ClassName: TopicServlet
 * @Description 主题相关的入口
 * @author muxue
 * @date Nov 30, 2015
 */
@WebServlet(urlPatterns="/topic/*")
public class TopicServlet extends BaseHttpServlet
{
	private static final long serialVersionUID = 1L;
	
	@SingleService
	TopicService topicService;
	
	// =====================================================================================
	// 主题列表
	// =====================================================================================
	
	/**
	 * 获取指定页的指定数目的topic记录，已测试
	 * errCode:
	 * 0：操作成功
	 * -1：数据库操作中出错
	 * -2：无效的class id，不能为负
	 * @param pageNum 当前页码，从1开始，是必要参数
	 * @param classId topics的类别id， 是必要参数
	 * @param countPerPage 一页多少，不是必要参数，默认每页10条记录
	 * @throws IOException 
	 */
	@ActionMapping("list")
	public void getTopicsByPageNum(@RequestParam("pageNum") long pageNum,
									@RequestParam("classId") int classId) throws IOException
	{
		if (classId <= 0) 
		{
			renderJSONResult(-2, "Invalid class id ...", null);
			return;
		}
		
		String countPerPageString = getParam("countPerPage");
		long countPerPage = 10L;
		
		if (ValidateUtil.validateLong(countPerPageString)) 
			countPerPage = Long.parseLong(countPerPageString);
		
		Page page = topicService.getTopicsInPage(pageNum, countPerPage, classId);
		
		if (page == null)
			renderJSONResult(-1, "something error while retriving page data ...", null);
		else
			renderJSONResult(0, copeTopicListResult(page));
	}
	
	// 转换名称以符合前台命名
	private Page copeTopicListResult(Page page)
	{
		ArrayList<HashMap<String, Object>> topics = page.getResult();
		ArrayList<HashMap<String, Object>> copedTopics = new ArrayList<>();
		
		for (HashMap<String,Object> topic : topics)
		{
			HashMap<String, Object> tempTopic = new HashMap<>();
			HashMap<String, Object> tempUser = new HashMap<>();
			HashMap<String, Object> tempRecent = new HashMap<>();
			tempTopic.put("topicId", topic.get("TopicID"));
			tempTopic.put("title", topic.get("TopicName"));
			tempTopic.put("title", topic.get("TopicName"));
			tempTopic.put("responseNum", topic.get("FloorsCount"));
			
			tempTopic.put("user", tempUser);
			tempUser.put("userId", topic.get("UserID"));
			tempUser.put("nickName", topic.get("NickName"));
			
			tempTopic.put("recent", tempRecent);
			tempRecent.put("userId", topic.get("RecentUserID"));
			tempRecent.put("nickName", topic.get("RecentName"));
			
			copedTopics.add(tempTopic);
		}
		
		page.setResult(copedTopics);
		return page;
	}
	
	
	/**
	 * 获取指定topic的基本信息，包括id和标题，已测试
	 * errCode:
	 * 0：操作成功
	 * -1：数据库操作中出错
	 * -2：无效的topic id，不能为负
	 * 
	 * @param topicId topic的id， 是必要参数
	 * @throws IOException 
	 */
	@ActionMapping("get")
	public void getTopicInfoById(@RequestParam("topicId") int topicId) throws IOException
	{
		if (topicId <= 0) 
		{
			renderJSONResult(-2, "Invalid topic id ...", null);
			return;
		}
		
		TopicBean topic = topicService.getTopicInfo(topicId);
		
		if (topic == null)
			renderJSONResult(-1, "something error while retriving page data ...", null);
		else
			renderJSONResult(0, copeTopicInfoResult(topic));
	}
	
	// 转换名称以符合前台命名
	private HashMap<String, Object> copeTopicInfoResult(TopicBean topic) throws IOException
	{
			HashMap<String, Object> tempTopic = new HashMap<>();
			tempTopic.put("topicId", topic.getTopicID());
			tempTopic.put("title", topic.getTopicName());
			return tempTopic;
	}
	
	
	// =====================================================================================
	// 新增主题
	// =====================================================================================
	
	/**
	 * 新增一个主题，未测试
	 * errCode:
	 * 0：操作成功
	 * -1：数据库操作中出错
	 * 
	 * @param title 主题名称，必要参数
	 * @param content 主题内容，必要参数
	 * @param classId 主题所属小类id，必要参数
	 * @throws IOException 
	 */
	@ActionMapping("new")
	@PermissionCheck(checker=LoginChecker.class)
	public void insertNewTopic(@RequestParam("title") String title,
								@RequestParam("content") String content,
								@RequestParam("classId") int classId) throws IOException
	{
		int userId = (int) getSessionAttr("userId");
		String userName = (String) getSessionAttr("userName");
		
		if (topicService.addNewTopic(title, content, classId, userId, userName))
			renderJSONResult(0, null);
		else 
			renderJSONResult(-1, "Unknown Error while coping ...", null);
	}
}
