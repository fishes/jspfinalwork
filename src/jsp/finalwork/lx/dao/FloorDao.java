/**
 * @FileName FloorDao.java
 * @Package jsp.finalwork.lx.dao
 * @author muxue
 * @date Dec 1, 2015
 */
package jsp.finalwork.lx.dao;

import tk.lx.dao.BaseDao;
import tk.lx.dao.mapping.FirstLowerMapping;

/**
 * @ClassName: FloorDao
 * @Description 帖子相关dao
 * @author muxue
 * @date Dec 1, 2015
 */
public class FloorDao extends BaseDao
{
	@Override
	protected void configResultMapping()
	{
		setResultMapping(new FirstLowerMapping());
	}
}
