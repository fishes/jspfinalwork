/**
 * @FileName ClassDao.java
 * @Package jsp.finalwork.lx.dao
 * @author muxue
 * @date Dec 6, 2015
 */
package jsp.finalwork.lx.dao;

import java.util.HashMap;

import tk.lx.dao.BaseDao;
import tk.lx.dao.mapping.FirstLowerMapping;

/**
 * @ClassName: ClassDao
 * @Description 类别相关dao
 * @author muxue
 * @date Dec 6, 2015
 */
public class ClassDao extends BaseDao
{
	@Override
	protected void configResultMapping()
	{
		setResultMapping(new FirstLowerMapping());
	}
	
	
	/**
	 * 判定指定大类class id是否存在
	 */
	public boolean isBigClassIdExist(int classId)
	{
		return selectOne("select ClassID from classtable"
						+ "where Belong=0") != null;
	}
	
	/**
	 * 判定指定小类class id是否存在
	 */
	public boolean isSmallClassIdExist(int classId)
	{
		return selectOne("select ClassID from classtable"
						+ "where Belong!=0") != null;
	}
	
	/**
	 * 判断某个主题是否属于某个小类
	 */
	public boolean isTopicInSmallClass(int topicId, int classId)
	{
		return selectOne("select TopicID "
						+ "from topictable "
						+ "where TopicID=? "
						+ "and ClassID=?", topicId, classId) != null;
	}
	
	/**
	 * 根据topic id获取小类id
	 */
	public int getSmallClassIdByTopicId(int topicId)
	{
		HashMap<String, Object> result = selectOne("select ClassID "
													+ "from topictable "
													+ "where TopicID=?", topicId);
		if (result != null) 
			return (int) result.get("ClassID");
		else 
			return Integer.MIN_VALUE;
	}
	
}
