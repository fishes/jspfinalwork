/**
 * @FileName TopicDao.java
 * @Package jsp.finalwork.lx.dao
 * @author muxue
 * @date Nov 30, 2015
 */
package jsp.finalwork.lx.dao;

import tk.lx.dao.BaseDao;
import tk.lx.dao.mapping.FirstLowerMapping;

/**
 * @ClassName: TopicDao
 * @Description Topic相关dao
 * @author muxue
 * @date Nov 30, 2015
 */
public class TopicDao extends BaseDao
{
	@Override
	protected void configResultMapping()
	{
		setResultMapping(new FirstLowerMapping());
	}
	
	/**
	 * 判断是否存在相应的TopicID
	 */
	public boolean isTopicIdValid(int topicId)
	{
		return selectOne("select TopicID from topictable"
						+ "where TopicID=?", topicId) != null;
	}
}
