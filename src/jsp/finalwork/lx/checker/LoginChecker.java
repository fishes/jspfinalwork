/**
 * @FileName LoginChecker.java
 * @Package jsp.finalwork.lx.checker
 * @author muxue
 * @date Dec 10, 2015
 */
package jsp.finalwork.lx.checker;

import tk.lx.permission.IRuleChecker;
import tk.lx.servlet.BaseHttpServlet;

/**
 * @ClassName: LoginChecker
 * @Description 用户登录检查
 * @author muxue
 * @date Dec 10, 2015
 */
public class LoginChecker implements IRuleChecker
{

	/* 
	 * 检查用户是否登录
	 */
	@Override
	public boolean checkRule(String rule, BaseHttpServlet servlet)
	{
		if (servlet.getSessionAttr("userId") == null) 
			return false;
		else 
			return true;
	}

}
