/**
 * @FileName FloorBean.java
 * @Package jsp.finalwork.lx.bean
 * @author muxue
 * @date Dec 1, 2015
 */
package jsp.finalwork.lx.bean;

import java.sql.Timestamp;

/**
 * @ClassName: FloorBean
 * @Description 帖子bean
 * @author muxue
 * @date Dec 1, 2015
 */
public class FloorBean
{
//	CREATE TABLE `floortable` (
//	  `FloorID` int(11) NOT NULL AUTO_INCREMENT COMMENT '楼层ID',
//	  `Content` longtext NOT NULL COMMENT '跟帖内容',
//	  `TopicID` int(11) NOT NULL COMMENT '主题ID',
//	  `FloorRegistTime` timestamp NOT NULL COMMENT '时间',
//	  `UserID` int(11) NOT NULL COMMENT '用户ID',
//	  `QuoteFloorID` int(11) NOT NULL COMMENT '引用楼层',
//	  `reserved1` varchar(255) DEFAULT NULL,
//	  `reserved2` varchar(255) DEFAULT NULL,
//	  PRIMARY KEY (`FloorID`)
//	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	
	private int floorID;
	private String content;
	private int topicID;
	private Timestamp floorRegistTime;
	private int userID;
	private int quoteFloorID;
	private String reserved1;
	private String reserved2;
	
	
	public int getFloorID()
	{
		return floorID;
	}
	public void setFloorID(int floorID)
	{
		this.floorID = floorID;
	}
	public String getContent()
	{
		return content;
	}
	public void setContent(String content)
	{
		this.content = content;
	}
	public int getTopicID()
	{
		return topicID;
	}
	public void setTopicID(int topicID)
	{
		this.topicID = topicID;
	}
	public Timestamp getFloorRegistTime()
	{
		return floorRegistTime;
	}
	public void setFloorRegistTime(Timestamp floorRegistTime)
	{
		this.floorRegistTime = floorRegistTime;
	}
	public int getUserID()
	{
		return userID;
	}
	public void setUserID(int userID)
	{
		this.userID = userID;
	}
	public int getQuoteFloorID()
	{
		return quoteFloorID;
	}
	public void setQuoteFloorID(int quoteFloorID)
	{
		this.quoteFloorID = quoteFloorID;
	}
	public String getReserved1()
	{
		return reserved1;
	}
	public void setReserved1(String reserved1)
	{
		this.reserved1 = reserved1;
	}
	public String getReserved2()
	{
		return reserved2;
	}
	public void setReserved2(String reserved2)
	{
		this.reserved2 = reserved2;
	}
}
