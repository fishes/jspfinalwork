/**
 * @FileName UserBean.java
 * @Package jsp.finalwork.lx.bean
 * @author muxue
 * @date Dec 1, 2015
 */
package jsp.finalwork.lx.bean;

import java.sql.Timestamp;

/**
 * @ClassName: UserBean
 * @Description 用户表
 * @author muxue
 * @date Dec 1, 2015
 */
public class UserBean
{
//	CREATE TABLE `user` (
//	  `UserID` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
//	  `UserName` varchar(255) NOT NULL COMMENT '用户姓名',
//	  `NickName` varchar(255) NOT NULL COMMENT '用户昵称',
//	  `PassWord` varchar(255) NOT NULL COMMENT '密码',
//	  `RegistTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '时间',
//	  `Email` varchar(255) DEFAULT NULL COMMENT '邮箱',
//	  `Level` varchar(255) DEFAULT NULL COMMENT '权限控制',
//	  `reserved1` varchar(255) DEFAULT NULL,
//	  `reserved2` varchar(255) DEFAULT NULL,
//	  `reserved3` varchar(255) DEFAULT NULL,
//	  PRIMARY KEY (`UserID`)
//	) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
	
	private int userID;
	private String userName;
	private String nickName;
	private String passWord;
	private Timestamp registTime;
	private String email;
	private String level;
	private String reserved1;
	private String reserved2;
	private String reserved3;
	
	
	public int getUserID()
	{
		return userID;
	}
	public void setUserID(int userID)
	{
		this.userID = userID;
	}
	public String getUserName()
	{
		return userName;
	}
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	public String getNickName()
	{
		return nickName;
	}
	public void setNickName(String nickName)
	{
		this.nickName = nickName;
	}
	public String getPassWord()
	{
		return passWord;
	}
	public void setPassWord(String passWord)
	{
		this.passWord = passWord;
	}
	public Timestamp getRegistTime()
	{
		return registTime;
	}
	public void setRegistTime(Timestamp registTime)
	{
		this.registTime = registTime;
	}
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	public String getLevel()
	{
		return level;
	}
	public void setLevel(String level)
	{
		this.level = level;
	}
	public String getReserved1()
	{
		return reserved1;
	}
	public void setReserved1(String reserved1)
	{
		this.reserved1 = reserved1;
	}
	public String getReserved2()
	{
		return reserved2;
	}
	public void setReserved2(String reserved2)
	{
		this.reserved2 = reserved2;
	}
	public String getReserved3()
	{
		return reserved3;
	}
	public void setReserved3(String reserved3)
	{
		this.reserved3 = reserved3;
	}
}
