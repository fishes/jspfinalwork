/**
 * @FileName TopicBean.java
 * @Package jsp.finalwork.lx.bean
 * @author muxue
 * @date Dec 1, 2015
 */
package jsp.finalwork.lx.bean;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * @ClassName: TopicBean
 * @Description 主题bean
 * @author muxue
 * @date Dec 1, 2015
 */
public class TopicBean
{
//	CREATE TABLE `topictable` (
//	  `TopicID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主题ID',
//	  `TopicName` varchar(255) NOT NULL COMMENT '主题名字',
//	  `TopicRegistTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '时间',
//	  `ClassID` int(11) NOT NULL COMMENT '小类ID',
//	  `UserID` int(11) NOT NULL COMMENT '用户ID',
//	  `RecentTime` date NOT NULL COMMENT '最近回复时间',
//	  `RecentName` varchar(255) NOT NULL COMMENT '最近回复人',
//	  `reserved1` varchar(255) DEFAULT NULL,
//	  `reserved2` varchar(255) DEFAULT NULL,
//	  `reserved3` varchar(255) DEFAULT NULL,
//	  PRIMARY KEY (`TopicID`)
//	) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
	
	private int topicID;
	private String topicName;
	private Timestamp topicRegistTime;
	private int classID;
	private int userID;
	private Date recentTime;
	private String recentName;
	private String reserved1;
	private String reserved2;
	private String reserved3;
	
	
	public int getTopicID()
	{
		return topicID;
	}
	public void setTopicID(int topicID)
	{
		this.topicID = topicID;
	}
	public String getTopicName()
	{
		return topicName;
	}
	public void setTopicName(String topicName)
	{
		this.topicName = topicName;
	}
	public Timestamp getTopicRegistTime()
	{
		return topicRegistTime;
	}
	public void setTopicRegistTime(Timestamp topicRegistTime)
	{
		this.topicRegistTime = topicRegistTime;
	}
	public int getClassID()
	{
		return classID;
	}
	public void setClassID(int classID)
	{
		this.classID = classID;
	}
	public int getUserID()
	{
		return userID;
	}
	public void setUserID(int userID)
	{
		this.userID = userID;
	}
	public Date getRecentTime()
	{
		return recentTime;
	}
	public void setRecentTime(Date resentTime)
	{
		this.recentTime = resentTime;
	}
	public String getRecentName()
	{
		return recentName;
	}
	public void setRecentName(String resentName)
	{
		this.recentName = resentName;
	}
	public String getReserved1()
	{
		return reserved1;
	}
	public void setReserved1(String reserved1)
	{
		this.reserved1 = reserved1;
	}
	public String getReserved2()
	{
		return reserved2;
	}
	public void setReserved2(String reserved2)
	{
		this.reserved2 = reserved2;
	}
	public String getReserved3()
	{
		return reserved3;
	}
	public void setReserved3(String reserved3)
	{
		this.reserved3 = reserved3;
	}
}
