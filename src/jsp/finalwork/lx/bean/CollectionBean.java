/**
 * @FileName CollectionBean.java
 * @Package jsp.finalwork.lx.bean
 * @author muxue
 * @date Dec 1, 2015
 */
package jsp.finalwork.lx.bean;


/**
 * @ClassName: CollectionBean
 * @Description 收藏bean
 * @author muxue
 * @date Dec 1, 2015
 */
public class CollectionBean
{
//	CREATE TABLE `collectiontable` (
//	  `CollectionID` int(11) NOT NULL AUTO_INCREMENT COMMENT '收藏ID',
//	  `UserID` int(11) NOT NULL COMMENT '用户ID',
//	  `TopicID` int(11) DEFAULT 0 COMMENT '主题ID',
//	  `ClassID` int(11) DEFAULT 0 COMMENT '小类ID',
//	  `reserved1` varchar(255) DEFAULT NULL,
//	  `reserved2` varchar(255) DEFAULT NULL,
//	  PRIMARY KEY (`CollectionID`)
//	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	
	private int collectionID;
	private int userID;
	private int topicID;
	private int classID;
	private String reserved1;
	private String reserved2;
	

	public int getCollectionID()
	{
		return collectionID;
	}
	public void setCollectionID(int collectionID)
	{
		this.collectionID = collectionID;
	}
	public int getUserID()
	{
		return userID;
	}
	public void setUserID(int userID)
	{
		this.userID = userID;
	}
	public int getTopicID()
	{
		return topicID;
	}
	public void setTopicID(int topicID)
	{
		this.topicID = topicID;
	}
	public int getClassID()
	{
		return classID;
	}
	public void setClassID(int classID)
	{
		this.classID = classID;
	}
	public String getReserved1()
	{
		return reserved1;
	}
	public void setReserved1(String reserved1)
	{
		this.reserved1 = reserved1;
	}
	public String getReserved2()
	{
		return reserved2;
	}
	public void setReserved2(String reserved2)
	{
		this.reserved2 = reserved2;
	}
}
