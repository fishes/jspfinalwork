/**
 * @FileName TopicService.java
 * @Package jsp.finalwork.lx.service
 * @author muxue
 * @date Nov 30, 2015
 */
package jsp.finalwork.lx.service;

import java.sql.Connection;

import jsp.finalwork.lx.bean.TopicBean;
import jsp.finalwork.lx.dao.ClassDao;
import jsp.finalwork.lx.dao.FloorDao;
import jsp.finalwork.lx.dao.TopicDao;
import tk.lx.annotation.SingleDao;
import tk.lx.dao.ITxCallback;
import tk.lx.dao.Page;
import tk.lx.service.BaseService;

/**
 * @ClassName: TopicService
 * @Description Topic相关服务
 * @author muxue
 * @date Nov 30, 2015
 */
public class TopicService extends BaseService
{
	@SingleDao
	private TopicDao topicDao;
	@SingleDao
	private FloorDao floorDao;
	@SingleDao
	private ClassDao classDao;
	
	/**
	 * 获取主题的基本信息
	 */
	public TopicBean getTopicInfo(int topicId)
	{
		return topicDao.selectBean(TopicBean.class, "select TopicID, TopicName "
													+ "from topictable "
													+ "where TopicId=?", topicId);
	}
	
	/**
	 * 获取主题分页数据
	 */
	public Page getTopicsInPage(long pageNum, long countPerPage, int classId)
	{
		return topicDao.paginate(pageNum, countPerPage, 
				"select t.TopicID, t.TopicName, t.UserID, t.TopicRegistTime, "
				+ "t.RecentUserID, t.RecentName, t.RecentTime, t.FloorsCount, "
				+ "u.NickName "
				+ "from topictable t, user u "
				+ "where t.UserID=u.UserID "
				+ "and t.ClassID=? "
				+ "order by t.RecentTime desc", classId);
	}
	
	/**
	 * 插入一个主题记录
	 */
	public boolean addNewTopic(final String title, final String content, 
						final int classId, final int userId, final String userName)
	{
		if (!classDao.isSmallClassIdExist(classId))
			return false;
		else
			return topicDao.tx(Connection.TRANSACTION_READ_COMMITTED, new ITxCallback()
			{
				@Override
				public boolean run()
				{
					// 插入一个新主题
					long newTopicId = topicDao.insert("insert into topictable ("
							+ "TopicName, TopRegistTime, ClassID, UserID, RecentTime, RecentName"
							+ ") values (?, CURRENT_TIMESTAMP(), ?, ?, NOW(), ?, ?)",
							title, classId, userId, userName);
					if(newTopicId == -1)
						return false;
	
					// 插入新主题的第一个楼层
					long newFloorId = floorDao.insert("insert into floortable ("
							+ "Content, TopicID, FloorRegistTime, UserID"
							+ ") values (?, ?, CURRENT_TIMESTAMP(), ?)",
							content, newTopicId, userId);
					if(newFloorId == -1)
						return false;
					
					// 更新用户的发表主题记录数
					if(floorDao.update("update user set TopicNum=TopicNum+1 where UserID=?", userId))
						return false;
					
					return true;
				}
			});
	}
}
