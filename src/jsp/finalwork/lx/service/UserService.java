/**
 * @FileName UserService.java
 * @Package jsp.finalwork.lx.service
 * @author muxue
 * @date Dec 1, 2015
 */
package jsp.finalwork.lx.service;

import java.util.HashMap;

import jsp.finalwork.lx.bean.UserBean;
import jsp.finalwork.lx.dao.ClassDao;
import jsp.finalwork.lx.dao.UserDao;
import tk.lx.annotation.SingleDao;
import tk.lx.dao.Page;
import tk.lx.dao.mapping.FirstLowerMapping;
import tk.lx.service.BaseService;

/**
 * @ClassName: UserService
 * @Description 用户相关service
 * @author muxue
 * @date Dec 1, 2015
 */
public class UserService extends BaseService
{
	@SingleDao
	private UserDao userDao;
	@SingleDao
	private ClassDao classDao;
	
	// =====================================================================================
	// 用户信息相关
	// =====================================================================================
	
	/**
	 * 获取用户基本信息
	 */
	public UserBean getBasicUserInfo(int userId)
	{
		return userDao.selectBean(UserBean.class, "select UserID, UserName, NickName, RegistTime, Email "
													+ "from user "
													+ "where UserID=?", userId);
	}
	
	/**
	 * 验证用户是否存在
	 */
	public boolean checkUser(String username, String password)
	{

		return userDao.selectOne("select * from user " 
								+ "where UserName=? "
								+ "and PassWord=?", 
								username, password) != null;
	}

	/**
	 * 判断用户名是否被占用
	 * true为用户名已被占用
	 */
	public boolean isUsernameUsed(String username)
	{
		HashMap<String, Object> result = userDao.selectOne("select count(*) count "
															+ "from user "
															+ "where UserName=?", username);
		return result != null && (long) result.get("count") > 0;
	}
	
	/**
	 * 获取需要放置在session中的数据，比如用户ID和用户等级，
	 * 用以进行是否登录判断，是否具有权限判断
	 */
	public UserBean getUserPutInSession(String username)
	{
		return userDao.selectBean(UserBean.class, "select * from user where UserName=?",
									username, new FirstLowerMapping());
	}
	
	// =====================================================================================
	// 用户发帖相关
	// =====================================================================================
	
	/**
	 * 获取用户发表过的帖子（floor）的信息：内容和id
	 */
	public Page getPostFloors(int userId, long currentPage)
	{
		return userDao.paginate(currentPage, "select f.Content, f.FloorID "
								+ "from user u, floortable f "
								+ "where u.UserID=f.UserID "
								+ "and u.UserID=?", userId);
	}
	
	// =====================================================================================
	// 关注主题相关
	// =====================================================================================
	
	/**
	 * 判断用户是否已经关注某个主题
	 * true为用户已经关注了指定主题
	 */
	public boolean isFavored(int userId, int topicId)
	{
		return userDao.selectOne("select TopicID "
								+ "from collectiontable "
								+ "where UserID=? "
								+ "and TopicID=?", userId, topicId) != null;
	}
	
	/**
	 * 增加用户对指定主题的关注
	 */
	public boolean addFavor(int userId, int topicId)
	{
		int classId = classDao.getSmallClassIdByTopicId(topicId);
		if (classId == Integer.MIN_VALUE) 
			return false;
		
		return userDao.insert("insert into collectiontable (UserID, TopicID, ClassID) "
							+ "values(?,?,?)", userId, topicId, classId) > 0;
	}
	
	/**
	 * 删除用户对指定主题的关注
	 */
	public boolean deleteFavor(int userId, int topicId)
	{
		return userDao.delete("delete from collectiontable "
								+ "where UserID=? "
								+ "and TopicID=?", userId, topicId);
	}
	
	/**
	 * 获取用户关注的主题列表
	 */
	public Page listFavors(int userId, long currentPageNum)
	{
		return userDao.paginate(currentPageNum, "select t.TopicID, t.TopicName "
												+ "from collectiontable c, topictable t "
												+ "where c.UserID=? "
												+ "and c.TopicID=t.TopicID", userId);
	}
	
	// =====================================================================================
	// 用户注册相关
	// =====================================================================================
	
	/**
	 * 插入一个新用户
	 * true表示插入成功
	 */
	public boolean insertNewUser(String username, String password, String nickname, String email)
	{
		long newUserId = userDao.insert("insert into user (UserName, PassWord, NickName, Email)"
										+ "values (?, ?, ?, ?)", username, password,nickname, email);
		return newUserId != -1;
	}
}
