/**
 * @FileName FloorService.java
 * @Package jsp.finalwork.lx.service
 * @author muxue
 * @date Dec 1, 2015
 */
package jsp.finalwork.lx.service;

import java.sql.Connection;

import jsp.finalwork.lx.dao.FloorDao;
import jsp.finalwork.lx.dao.TopicDao;
import tk.lx.annotation.SingleDao;
import tk.lx.dao.ITxCallback;
import tk.lx.dao.Page;
import tk.lx.service.BaseService;

/**
 * @ClassName: FloorService
 * @Description 帖子相关service
 * @author muxue
 * @date Dec 1, 2015
 */
public class FloorService extends BaseService
{
	@SingleDao
	private FloorDao floorDao;
	@SingleDao
	private TopicDao topicDao;
	
	/**
	 * 根据主题ID和页码获取指定页面的数据
	 * @param currentPage
	 * @param topicId
	 * @return
	 */
	public Page getFloorsInPage(int currentPage, int topicId)
	{
		return floorDao.paginate(currentPage, 20, "select f.Content, "
											+ "u.userID, u.NickName, u.RegistTime, u.TopicNum "
											+ "from floortable f, user u "
											+ "where f.TopicID= ? "
											+ "and f.UserID=u.UserID "
											+ "order by f.FloorRegistTime", topicId);
	}
	
	
	/**
	 * 插入一条新floor，没有处理QuoteFloorId
	 */
	public boolean addNewFloor(final String content, final int topicId, 
								final int userId, final String userName, final int quoteFloorId)
	{
		if (!topicDao.isTopicIdValid(topicId))
			return false;
		else
			return topicDao.tx(Connection.TRANSACTION_READ_COMMITTED, new ITxCallback()
			{
				@Override
				public boolean run()
				{
					long newFloorId = floorDao.insert("insert into floortable ("
							+ "Content, TopicID, FloorRegistTime, UserID, QuoteFloorID"
							+ ") values (?, ?, CURRENT_TIMESTAMP(), ?, ?)",
							content, topicId, userId, quoteFloorId);
					if(newFloorId == -1)
						return false;
					
					return topicDao.update("update topictable set "
									+ "RecentTime=NOW(), RecentName=?, FloorsCount=FloorsCount+1 "
									+ "where TopicID=?", userName, topicId);
				}
			});
	}
}
