package tk.lx.dao.helper;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import tk.lx.dao.mapping.IResultMapping;

/**
 * ClassName: ModelToBeanUtil
 * Description:	将数据库返回通用性的结果集合转成bean集合
 * 				（需要你自己清楚知道是可转的，也就是原来的查询语句是处理单表的）
 * 				（包括后面可能一些东西需要强转类型，你自己也要清楚） 
 * Date: Oct 31, 2015
 *
 * @author muxue
 */

public class RecordBeanConverter
{
	/**
	 * 尝试将单表查询结果转换成bean集合，方便操作
	 */
	public static <T> ArrayList<T> convertRecordsToBeans(ArrayList<HashMap<String, Object>> records, Class<T> clazz, IResultMapping resultMapping)
	{
		if (records == null || clazz == null || resultMapping == null)
			return null;
		
		ArrayList<T> list = new ArrayList<>();
		boolean isEmptyList = true;
		try
		{
			for(HashMap<String, Object> record : records)
			{
				T bean = convertRecordToBean(record, clazz, resultMapping);
				if (bean != null)
				{
					list.add(bean);
					isEmptyList = false;
				}
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
		
		return isEmptyList ? null : list;
	}
	
	
	/**
	 * 转换一条record到bean
	 */
	public static <T> T convertRecordToBean(HashMap<String, Object> record, Class<T> clazz, IResultMapping resultMapping)
	{
		if (record == null || clazz == null || resultMapping == null)
			return null;
		
		T bean = null;
		boolean isPropertyExist = false;
		try
		{
			bean = clazz.newInstance();
			for (Entry<String, Object> entry : record.entrySet())
			{
				String beanFieldName = resultMapping.getCoName(entry.getKey());
				Field[] fields = bean.getClass().getDeclaredFields();
				for (Field field : fields)
					if(field.getName().equals(beanFieldName))
					{
						isPropertyExist = true;
						field.setAccessible(true);
						//其实这里应该是不要考虑的了，如果类型不兼容的话肯定会报错
						//但是，使java对象里的类型和数据库表字段类型兼容是使用者应当保证的
						//
						//如果数据库方面是int型的，而java这面是short型的，是不能成功set的
						//反过来可以应该是因为JDBC兼容性，即使是smallint型，但对应java类型是int的
						//
						//应当参考具体JDBC驱动的实现文档。这里所做的都以MySQL为准。
						//
						//FIXME:但是是不是允许自动转换一些类型
						field.set(bean, entry.getValue());
					}
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
		
		return isPropertyExist ? bean : null;
	}
	
}
