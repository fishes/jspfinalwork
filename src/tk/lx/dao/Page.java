package tk.lx.dao;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * ClassName: Page
 * Description: 封装分页结果 
 * Date: Oct 31, 2015
 *
 * @author muxue
 */

public class Page
{
	//记录总数
	private long recordTotalCount = 0;
	//总共有多少记录页
	private long totalPages = 0;
	//要求多少记录每页
	private long countPerPage = 1;
	//当前页实际有多少条记录
	private int countCurPage = 0;
	//当前页
	private long currentPage = 1;
	//当前页的数据
	private ArrayList<HashMap<String, Object>> result = null;
	
	
	//============================================================================
	//get/set
	
	public long getRecordTotalCount()
	{
		return recordTotalCount;
	}
	
	public void setRecordTotalCount(long recordTotalCount)
	{
		this.recordTotalCount = recordTotalCount;
	}
	
	public ArrayList<HashMap<String, Object>> getResult()
	{
		return result;
	}
	
	public void setResult(ArrayList<HashMap<String, Object>> result)
	{
		this.result = result;
		if (result != null)
			this.countCurPage = result.size();
	}
	
	public long getTotalPages()
	{
		return totalPages;
	}
	
	public void setTotalPages(long totalPage)
	{
		this.totalPages = totalPage;
	}

	public long getCurrentPage()
	{
		return currentPage;
	}

	public void setCurrentPage(long currentPage)
	{
		this.currentPage = currentPage;
	}

	public long getCountPerPage()
	{
		return countPerPage;
	}

	public void setCountPerPage(long countPerPage)
	{
		this.countPerPage = countPerPage;
	}

	public int getCountCurPage()
	{
		return countCurPage;
	}

	public void setCountCurPage(int countCurPage)
	{
		this.countCurPage = countCurPage;
	}
}
