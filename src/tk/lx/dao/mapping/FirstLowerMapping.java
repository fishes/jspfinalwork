/**
 * @FileName FirstLowerMapping.java
 * @Package tk.lx.dao.mapping
 * @author muxue
 * @date Dec 2, 2015
 */
package tk.lx.dao.mapping;

/**
 * @ClassName: FirstLowerMapping
 * @Description 
 * @author muxue
 * @date Dec 2, 2015
 */
public class FirstLowerMapping implements IResultMapping
{

	/* 
	 * 第一个字符转成小写，其他保持不变
	 */
	@Override
	public String getCoName(String columnName)
	{
		if(columnName != null && !"".equals(columnName))
			return columnName.substring(0, 1).toLowerCase()
					+ columnName.substring(1);

		return null;
	}

}
