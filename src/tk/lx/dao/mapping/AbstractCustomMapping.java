/**
 * @FileName AbstractCustomMapping.java
 * @Package tk.lx.dao.helper
 * @author muxue
 * @date Nov 12, 2015
 */
package tk.lx.dao.mapping;

import java.util.HashMap;

/**
 * @ClassName: AbstractCustomMapping
 * @Description 抽象的自定义映射，使用者继承本类后使用addMappingX方法添加映射即可
 * @author muxue
 * @date Nov 12, 2015
 */
public abstract class AbstractCustomMapping implements IResultMapping
{
	private HashMap<String, String> mappings = null;
	
	/* 
	 * 
	 */
	@Override
	public String getCoName(String columnName)
	{
		if (mappings == null)
		{
			mappings = new HashMap<>();
			makeMappings();
		}
		return mappings.get(columnName);
	}

	
	/**
	 * 给继承类添加映射的接口
	 */
	public abstract void makeMappings();
	
	
	/**
	 * 添加单个映射 
	 */
	public AbstractCustomMapping addMapping(String columnName, String propertyName)
	{
		if (columnName != null && propertyName != null)
			mappings.put(columnName, propertyName);

		return this;
	}
	
	/**
	 * 批添加映射 
	 */
	public AbstractCustomMapping addMappings(HashMap<String, String> mappings)
	{
		this.mappings.putAll(mappings);
		return this;
	}
	
	
	public HashMap<String, String> getMappings()
	{
		return mappings;
	}
	
	public void setMappings(HashMap<String, String> mappings)
	{
		this.mappings = mappings;
	}
}
