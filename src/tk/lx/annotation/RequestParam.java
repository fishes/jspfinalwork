/** 
 * Project Name: TheSimple 
 * File Name: RequestParam.java 
 * Package Name: tk.lx.annotation 
 */ 
package tk.lx.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ClassName: RequestParam
 * Description: 要求相应参数具有值
 * Date: Nov 1, 2015
 *
 * @author muxue
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestParam
{
	String value();
}
