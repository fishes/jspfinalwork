/**
 * Project Name: TheSimple
 * File Name: ActionName.java
 * Package Name: tk.lx.annotation
 */
package tk.lx.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ClassName: ActionName
 * Description: 
 * Date: Oct 31, 2015
 *
 * @author muxue
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ActionMapping
{
	String value();
}
