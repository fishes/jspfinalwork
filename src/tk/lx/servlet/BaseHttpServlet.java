package tk.lx.servlet;

/**
 * 包装HttpServlet，实现dispatch，方便后续开发
 * 继承的类只需要写相应的处理方法就好
 * 
 * @author lx
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import tk.lx.annotation.ActionMapping;
import tk.lx.annotation.PermissionCheck;
import tk.lx.annotation.RequestParam;
import tk.lx.dao.Page;
import tk.lx.permission.IRuleChecker;
import tk.lx.servlet.helper.StringConverter;
import tk.lx.servlet.helper.UploadFileInfo;
import tk.lx.util.BeanFactoryUtil;
import tk.lx.util.JsonUtil;
import tk.lx.util.UploadUtil;


public abstract class BaseHttpServlet extends HttpServlet
{

	private static final long serialVersionUID = 1L;

	//出于多线程并发考虑
	private ThreadLocal<HttpServletRequest> request = new ThreadLocal<>();
	private ThreadLocal<HttpServletResponse> response = new ThreadLocal<>();
	private ThreadLocal<HttpSession> session = new ThreadLocal<>();
	private ThreadLocal<HashMap<String, Object>> paramsPostByJson = new ThreadLocal<>();
	private ServletContext context = null;
	
	//URL与Method（Action）的映射，只在init中写入
	private HashMap<String, Method> actionMap = new HashMap<>();
	
	private IRuleChecker permissionChecker = new IRuleChecker()
	{
		
		@Override
		public boolean checkRule(String rule, BaseHttpServlet servlet)
		{
			return true;
		}
	};
	private boolean copeWithJSONByDefault = true;
	
	
	public BaseHttpServlet()
	{
		// inject the services & daos
		BeanFactoryUtil.injectFields(this);
		config();
	}
	
	@Override
	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);
		if (context == null)
			this.context = config.getServletContext();
		
		Method[] methods = this.getClass().getDeclaredMethods();

		for (Method method : methods)
		{
			String beCalledName = method.getName();
			
			//@ActionName优先起作用
			//Attention: 这里是没有去处理重复映射的问题的，所以如果有一样的映射名，那么调用哪个方法是不确定的
			ActionMapping actionMapping = method.getAnnotation(ActionMapping.class);
			if (actionMapping != null)
				beCalledName = actionMapping.value();
			
			int modifier = method.getModifiers();
			if (Modifier.isPublic(modifier) && "void".equals(method.getReturnType().getName()))
				actionMap.put(beCalledName, method);
		}
	}
	
	/**
	 * 如果子类需要对BaseHttpServlet进行配置则覆盖此方法
	 */
	protected void config()
	{
	}
	
	//==================================================================================
	
	/**
	 * 如果要在一个servlet的范围中使用一个自定义的权限检查器
	 * 那么继承类可以覆盖config方法，自己进行配置，否则父类将提供一个认为所有权限都OK的检查器
	 * setPermissionChecker(new IRuleChecker(){
	 * 		public boolean checkRule(String rule){
	 * 			...
	 * 		}
	 * });
	 * 如果在具体的action上的@PermissionCheck注解中传入了另外的权限检查器，则优先使用之
	 */
	protected synchronized void setPermissionChecker(IRuleChecker permissionChecker)
	{
		if (permissionChecker != null)
			this.permissionChecker = permissionChecker;
	}
	
	
	//也许有内存泄漏问题，但不是很觉得，因为servlet的撤销时机，不过也还是补上吧
	@Override
	public void destroy()
	{
		if (request != null)
			request.remove();
		if (response != null)
			response.remove();
		if (session != null)
			session.remove();
	}
	
	//视图路径前缀，默认就是"/WEB-INF/jsp"
	private String baseViewPath = "/WEB-INF/jsp";
	
	protected synchronized void setBaseViewPath(String baseViewPath)
	{
		this.baseViewPath = baseViewPath;
	}
	
	private String addBaseViewPath(String relativePath)
	{
		if (relativePath == null)
			return "";
		
		//不以'/'开头的且有'.'则认为是相对路径，做个转换
		if (!relativePath.startsWith("/") && relativePath.contains("."))
			return getBaseViewPath() + "/" + relativePath;
		else
			return relativePath;
	}
	
	/**
	 * 子类就不需要再写doGet()了也不能写了，除非自己去处理那一整套东西，与其这样，不继承本类即可
	 */
	@Override
	public final void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * 子类就不需要再写doPost()也不能写了，除非自己去处理那一整套东西，与其这样，不继承本类即可
	 */
	@Override
	public final void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.request.set(request);
		this.response.set(response);
		this.session.set(request.getSession());
		dispatch();
	}

	/**
	 * 根据额外的pathinfo来分发处理的action
	 * 没有去处理重复映射的问题
	 * 所以如果有一样的映射名，那么调用哪个方法是不确定的
	 */
	private void dispatch() throws IOException
	{

		String methodParam = getRequest().getPathInfo();
		
		//路径有'.'，就认为是具体文件，不处理，直接跳过
		//
		//====================== Bug Declaration =====================
		//下面的语句处理存在bug，现阶段的条件（保持使用servlet）无法修复
		//说明如下：
		//		如果存在与serlvet映射（../*形式）匹配的静态文件，是不能被访问到的
		//		问题出在../*的映射上，如果都是普通的完全匹配的路径的话，那么是没有问题的
		//		jsp或者其他静态文件请求都能够进入正确的servlet中得到处理
		//		另外的处理方案可以使用filter，因为它可以控制请求是否往下流转
		//		下面的判断其实也没有什么作用，只能报个错了
		// 		目前经济且便捷的方式就是不要使用“两边冲突”的路径吧
		if(methodParam != null && methodParam.contains("."))
		{
			if (copeWithJSONByDefault)
				renderJSONError("This actually is a bug, but now I can or would not fix it."
								+ " You may change your static resource path or the servlet path"
								+ " to avoid the same matching. Thank You ...");
			else
				renderToErrorPage("This actually is a bug, but now I can or would not fix it."
								+ " You may change your static resource path or the servlet path"
								+ " to avoid the same matching. Thank You ...");
			return;
		}
		//====================== End Bug Declaration ==================
		
		
		if (methodParam == null || "".equals(methodParam) || "/".equals(methodParam))
			methodParam = "/index";
		methodParam = methodParam.substring(1);
		Method method = actionMap.get(methodParam);
		
		// 指定方法是否已经被调用
		boolean invoked = false;

		do
		{
			if (method != null)
			{
				//检查权限
				if (!checkPermission(method))
					break;

				//调用前检查是否需要注入参数
				Object[] params = injectParams(method);
				if (params == null)
					return;

				try
				{
					invoked = true;
					method.invoke(this, params);
				}
				catch (Exception ex)
				{
					ex.printStackTrace();
					if (copeWithJSONByDefault)
						renderJSONError("Something wrong while invoking ...");
					else
						renderToErrorPage("Something wrong while invoking ...");
				}
			}
		}
		while (false);
		
		// 清楚本次post请求中的json数据
		paramsPostByJson.remove();
		
		// 没有可以处理请求的method || 没有权限
		if (!invoked)
		{
			if (copeWithJSONByDefault)
				renderJSONError("Invalid privilege or no such method ...");
			else
				renderToErrorPage("Invalid privilege or no such method ...");
		}
	}
	
	/**
	 * 检查权限 
	 */
	private boolean checkPermission(Method method)
	{
		PermissionCheck permissionCheck = method.getAnnotation(PermissionCheck.class);
		if (permissionCheck != null)
		{
			String rule = permissionCheck.rule();
			Class<? extends IRuleChecker>[] checkerClasses = permissionCheck.checker();
			boolean ifUseGlobalChecker = permissionCheck.useDefault();
			
			if (ifUseGlobalChecker && !permissionChecker.checkRule(rule, this)) 
				return false;
			
			for (Class<? extends IRuleChecker> checkerClass : checkerClasses)
			{
				if (checkerClass != IRuleChecker.class)
					try
					{
						if (!checkerClass.newInstance().checkRule(rule, this))
							return false;
					}
					catch (InstantiationException | IllegalAccessException e)
					{
						e.printStackTrace();
						return false;
					}
			}
		}
		
		return true;
	}
	
	/**
	 * 注入action的参数
	 */
	private Object[] injectParams(Method method) throws IOException
	{
		Annotation[][] paramAnnotations = method.getParameterAnnotations();
		Object[] params = new Object[paramAnnotations.length];
		Class<?>[] paramTypes = method.getParameterTypes();
		
		for (int index = 0; index < paramAnnotations.length; index++)
		{
			if (paramAnnotations[index].length <= 0)
			{
				//没有注解，传递一个类型的默认值
				params[index] = StringConverter.getDefaultValue(paramTypes[index]);
				continue;
			}
			// 要求@RequestParam注解在第一个位置
			Class<? extends Annotation> annotation = paramAnnotations[index][0].annotationType();
			
			if ("tk.lx.annotation.RequestParam".equals(annotation.getName()))
			{
				String value = getParam(((RequestParam)(paramAnnotations[index][0])).value());
				Object targetValue = null;
				if (value != null && !"".equals(value))
				{
					targetValue = StringConverter.str2object(value.trim(), paramTypes[index]);
					// 如果请求中的参数不符合目的参数的类型，则直接报错
					if (StringConverter.isConvertSucceed) 
					{
						params[index] = targetValue;
						continue;
					}
				}
				if (copeWithJSONByDefault)
					renderJSONError("The request parameter is null or not in correct form");
				else
					renderToErrorPage("The request parameter is null or not in correct form");
				return null;
			}
			else 
				//不是上面两个注解的话就传递一个类型的默认值，防止下面invoke出错
				params[index] = StringConverter.getDefaultValue(paramTypes[index]);
		}
		
		return params;
	}
	
	
	// ==============================================================================
	// 针对request，response，session，context的封装
	// ===============================================================================

	/**
	 * 客户端跳转
	 */
	protected void sendRedirect(String path)
	{
		try
		{
			getResponse().sendRedirect(getRequest().getContextPath() + addBaseViewPath(path));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	
	/**
	 * 服务端跳转
	 */
	protected void forward(String path)
	{
		try
		{
			getRequest().getRequestDispatcher(addBaseViewPath(path)).forward(getRequest(), getResponse());
		}
		catch (ServletException | IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * 编码客户端跳转链接
	 */
	public String encodeRedirectURL(String url)
	{
		return getResponse().encodeRedirectURL(url);
	}
	
	/**
	 * 编码服务端跳转链接
	 */
	public String encodeURL(String url)
	{
		return getResponse().encodeURL(url);
	}
	
	//===================================================================================================
	
	/**
	 * 设置指定request属性
	 */
	protected void setAttr(String key, Object value)
	{
		getRequest().setAttribute(key, value);
	}

	/**
	 * 获取指定request属性
	 */
	public Object getAttr(String key)
	{
		return getRequest().getAttribute(key);
	}

	/**
	 * 移除指定request属性
	 */
	protected void rmAttr(String key)
	{
		getRequest().removeAttribute(key);
	}

	//===================================================================================================
	
	/**
	 * 设置指定session属性
	 */
	protected void setSessionAttr(String key, Object value)
	{
		getSession().setAttribute(key, value);
	}

	/**
	 * 获取指定session属性
	 */
	public Object getSessionAttr(String key)
	{
		return getSession().getAttribute(key);
	}

	/**
	 * 移除指定session属性
	 */
	protected void rmSessionAttr(String key)
	{
		getSession().removeAttribute(key);
	}

	//===================================================================================================
	
	/**
	 * 设置context属性
	 */
	protected void setContextAttr(String key, Object value)
	{
		getContext().setAttribute(key, value);
	}

	/**
	 * 获取指定context属性
	 */
	public Object getContextAttr(String key)
	{
		return getContext().getAttribute(key);
	}

	/**
	 * 移除指定context属性
	 */
	protected void rmContextAttr(String key)
	{
		getContext().removeAttribute(key);
	}
	
	//===================================================================================================
	
	/**
	 * 取得所有cookie
	 */
	public Cookie[] getCookies()
	{
		return getRequest().getCookies();
	}
	
	/**
	 * 取得指定名称cookie
	 */
	public Cookie getCookie(String cookieName)
	{
		if (cookieName == null)
			return null;
		
		Cookie[] cookies = getCookies();
		if (cookies == null)
			return null;
		
		for (Cookie cookie : cookies)
			if (cookieName.equals(cookie.getName()))
				return cookie;
		
		return null;
	}
	
	
	/**
	 * 取得指定名称cookie的value
	 */
	public String getCookieValue(String cookieName)
	{
		Cookie cookie = getCookie(cookieName);
		if (cookie == null)
			return null;
		
		return cookie.getValue();
	}
	
	/**
	 * 添加一个cookie
	 */
	protected void addCookie(Cookie cookie)
	{
		getResponse().addCookie(cookie);
	}
	
	//===================================================================================================
	
	/**
	 * 获取request的header
	 */
	public String getReqHeader(String headerName)
	{
		return getRequest().getHeader(headerName);
	}
	
	/**
	 * 获取request的header的多值
	 */
	public List<String> getReqHeaders(String headerName)
	{
		List<String> headers = new Vector<>();
		Enumeration<String> enums = getRequest().getHeaders(headerName);
		
		while (enums.hasMoreElements())
		{
			String header = enums.nextElement();
			headers.add(header);
		}
		
		return headers;
	}
	
	
	/**
	 * 获取request所有header的名称
	 */
	public List<String> getReqHeaderNames()
	{
		List<String> headerNames = new Vector<>();
		Enumeration<String> enums = getRequest().getHeaderNames();
		
		while (enums.hasMoreElements())
		{
			String header = enums.nextElement();
			headerNames.add(header);
		}
		
		return headerNames;
	}
	
	
	/**
	 * 获取response的header
	 */
	public String getResHeader(String headerName)
	{
		return getResponse().getHeader(headerName);
	}
	
	/**
	 * 获取response的header的多值
	 */
	public List<String> getResHeaders(String headerName)
	{
		List<String> headers = new Vector<>();
		Collection<String> colls = getResponse().getHeaders(headerName);
		Iterator<String> iter = colls.iterator();
		
		while (iter.hasNext())
			headers.add(iter.next());
		
		return headers;
	}
	
	/**
	 * 获取response所有header的名称
	 */
	public List<String> getResHeaderNames()
	{
		List<String> headerNames = new Vector<>();
		Collection<String> colls = getResponse().getHeaderNames();
		Iterator<String> iter = colls.iterator();
		
		while (iter.hasNext())
			headerNames.add(iter.next());
		
		return headerNames;
	}
	
	/**
	 * 添加header
	 */
	protected void addHeader(String headerName, String headerValue)
	{
		getResponse().addHeader(headerName, headerValue);
	}
	
	/**
	 * 是否存在某个header
	 */
	public boolean containsHeader(String headerName)
	{
		return getResponse().containsHeader(headerName);
	}
	
	/**
	 * 设置response的header
	 */
	protected void setHeader(String headerName, String headerValue)
	{
		getResponse().setHeader(headerName, headerValue);
	}
	
	
	//===================================================================================================
	
	/**
	 * 获取请求方法名称
	 */
	public String getMethod()
	{
		return getRequest().getMethod();
	}
	
	
	/**
	 * 获取上下文路径
	 */
	public String getContextPath()
	{
		return getRequest().getContextPath();
	}
	
	
	/**
	 * 获取查询字串
	 */
	public String getQueryString()
	{
		return getRequest().getQueryString();
	}
	
	
	/**
	 * 获取request
	 */
	protected HttpServletRequest getRequest()
	{
		return request.get();
	}
	
	/**
	 * 获取session
	 */
	protected HttpSession getSession()
	{
		return session.get();
	}
	
	/**
	 * 获取response
	 */
	protected HttpServletResponse getResponse()
	{
		return response.get();
	}
	
	/**
	 * 获取context
	 */
	protected ServletContext getContext()
	{
		return context;
	}
	
	//===================================================================================================
	
	/**
	 * 获取part
	 * @throws ServletException 
	 * @throws IOException 
	 */
	protected Part getPart(String partName) throws IOException, ServletException
	{
		return getRequest().getPart(partName);
	}
	
	/**
	 * 获取parts
	 * @throws ServletException 
	 * @throws IOException 
	 */
	protected List<Part> getParts() throws IOException, ServletException
	{
		List<Part> parts = new Vector<>();
		Iterable<Part> iters = getRequest().getParts();

		for (Part iter1 : iters) 
			parts.add(iter1);
		
		return parts;
	}
	
	/**
	 * 这里指示的路径必须存在，不然会报错。（这里就不依赖程序去创建了）
	 * slash统一用的是正斜杠"/"
	 */
	private String uploadFilePath = "/uploadFiles/";
	
	/**
	 * 保存指定的part文件，文件名称随机 （扩展名沿用）
	 * slash统一用的是正斜杠"/"
	 * @throws IOException
	 * @throws ServletException
	 */
	protected UploadFileInfo saveFile(String partName) throws IOException, ServletException
	{
		Part part = getPart(partName);
		if (part == null)
			return null;
		String extendName = UploadUtil.getFileType(part);
		
		if (isFileTypeAllowed(extendName))
		{
			String filename = UploadUtil.getRandomFileName() + UploadUtil.getFileType(part);
			part.write(getServletContext().getRealPath(uploadFilePath) 
						+ "/" + filename);
			
			return new UploadFileInfo(filename, UploadUtil.getFileName(part),
					part.getSize(), extendName, uploadFilePath + filename);
		}
		
		return null;
	}
	
	
	/**
	 * 保存指定的part文件，文件名称由filename指定（包括扩展名）
	 * slash统一用的是正斜杠"/"
	 * @throws IOException
	 * @throws ServletException
	 */
	protected UploadFileInfo saveFile(String partName, String fileName) throws IOException, ServletException 
	{
		Part part = getPart(partName);
		if (part == null || fileName == null || "".equals(fileName))
			return null;

		part.write(getServletContext().getRealPath(uploadFilePath) 
					+ "/" + fileName);
		String originalName = UploadUtil.getFileName(part);
		return new UploadFileInfo(originalName, fileName,
				part.getSize(), UploadUtil.getFileType(part), uploadFilePath + fileName);
	}
	
	/**
	 * 保存所有文件，文件名称使用随机的，扩展名沿用 
	 * slash统一用的是正斜杠"/"
	 * @throws IOException
	 * @throws ServletException
	 */
	protected ArrayList<UploadFileInfo> saveFiles() throws IOException, ServletException 
	{
		ArrayList<UploadFileInfo> filesInfo = new ArrayList<>();
		UploadFileInfo tempFilename = null;
		for (Part part : getParts())
		{
			tempFilename = saveFile(part.getName());
			if (tempFilename != null)
				filesInfo.add(tempFilename);
		}
		
		return filesInfo;
	}
	
	
	/**
	 * 文件类型简单过滤，这里就全部允许，子类可以考虑覆盖此方法
	 */
	protected boolean isFileTypeAllowed(String extendName)
	{
		return true;
	}
	//===================================================================================================
	
	/**
	 * 获取编码方式
	 */
	public String getCharacterEncoding()
	{
		return getRequest().getCharacterEncoding();
	}

	/**
	 * 设置编码方式
	 */
	protected void setCharacterEncoding(String encoding)
	{
		getResponse().setCharacterEncoding(encoding);
	}
	
	/**
	 * 获取文档类型
	 */
	public String getContentType()
	{
		return getRequest().getContentType();
	}
	
	/**
	 * 设置文档类型
	 */
	protected void setContentType(String contentType)
	{
		getResponse().setContentType(contentType);
	}
	
	/**
	 * 获取区域
	 */
	public Locale getLocale()
	{
		return getRequest().getLocale();
	}
	
	/**
	 * 设置区域
	 */
	protected void setLocale(Locale locale)
	{
		getResponse().setLocale(locale);
	}
	
	/**
	 * 设置session失效
	 */
	protected void invalidSession()
	{
		getSession().invalidate();
	}
	
	/**
	 * 设置http状态
	 */
	protected void setStatus(int st)
	{
		getResponse().setStatus(st);
	}
	
	/**
	 * 获取某个参数
	 * 支持从JSON数据中获取参数
	 * 暂不支持参数层级的叠加，比如user.name，而user也是一个对象
	 * 如果URL中参数和JSON数据中的参数名称冲突，则取URL中的参数值
	 * @throws IOException 
	 */
	public String getParam(String key) throws IOException
	{
		String value = getRequest().getParameter(key);
		if (value != null) 
			return value.trim();
		
		HashMap<String, Object> paramsInThread = paramsPostByJson.get();
		if (paramsInThread == null) 
		{
			StringBuilder builder = new StringBuilder();
			
			// 获取JSON中的参数
			if (getContentType() != null && getContentType().contains("application/json")) 
			{
				BufferedReader reader = getRequest().getReader();
				if (reader == null) 
					return null;
				
				String tempstr = reader.readLine();
				for (; tempstr != null; tempstr = reader.readLine()) 
					builder.append(tempstr);
			}

			HashMap<String, Object> paramsInJson = JsonUtil.parseJsonToMap(builder.toString());
			if (paramsInJson != null) 
			{
				paramsPostByJson.set(paramsInJson);
				return paramsInJson.get(key) == null ? null : paramsInJson.get(key).toString();
			}
			else 
				return null;
		}
		
		return paramsInThread.get(key) == null ? null : paramsInThread.get(key).toString();
	}

	/**
	 * 获取参数map
	 */
	public Map<String, String[]> getParmsMap()
	{
		return getRequest().getParameterMap();
	}
	
	// ==============================================================================
	// 另外的工具方法
	// ===============================================================================
	
	// ==JSON化数据===================================================================

	/**
	 * 向浏览器返回json数据
	 */
	protected void renderJSON(Object object) throws IOException
	{
		PrintWriter out = getResponse().getWriter();
		getResponse().setHeader("Content-Type", "application/json");
		out.write(JsonUtil.stringify(object));
		out.flush();
		out.close();
	}
		
	
	/**
	 * JSON形式返回出错信息
	 * {"errCode": Integer.MIN_VALUE, "errMsg": ...}
	 */
	protected void renderJSONError(String errMsg) throws IOException
	{
		renderJSONError(Integer.MIN_VALUE, errMsg);
	}
	
	/**
	 * JSON形式返回出错信息
	 * {"errCode": ..., "errMsg": ...}
	 */
	protected void renderJSONError(int errCode, String errMsg) throws IOException
	{
		Map<String, Object> result = new LinkedHashMap<>();
		result.put("errCode", errCode);
		result.put("errMsg", errMsg);
		renderJSON(result);
	}

	/**
	 * JSON形式返回通用结果,errMsg为null
	 * {"errCode": ..., "errMsg": null, "data": ...}
	 */
	protected void renderJSONResult(int errCode, Object data) throws IOException
	{
		renderJSONResult(errCode, null, data);
	}
	
	/**
	 * JSON形式返回通用结果,errMsg为null
	 * {"errCode": ..., "errMsg": null, "data": ...}
	 */
	protected void renderJSONResult(int errCode, String errMsg) throws IOException
	{
		renderJSONResult(errCode, errMsg, null);
	}
	
	/**
	 * JSON形式返回通用结果
	 * {"errCode": ..., "errMsg": ..., "data": ...}
	 */
	protected void renderJSONResult(int errCode, String errMsg, Object data) throws IOException
	{
		Map<String, Object> result = new LinkedHashMap<>();
		result.put("errCode", errCode);
		result.put("errMsg", errMsg);
		result.put("data", data);
		renderJSON(result);
	}
	

	/**
	 * JSON形式返回UMEditor所需要的数据
	 * 
	 * 虽然返回的是json形式，但是一定要将Content-Type设置为text/html
	 * 不然手动点击上传的时候会出错，尽管文件是已经上传成功的，但是拖拽上传却是可以的
	 * 关于MIME再做个深入研究吧
	 * @param fileInfo
	 * @param state 返回的状态
	 * @throws IOException
	 * @throws ServletException
	 */
	protected void renderUMResult(UploadFileInfo fileInfo, String state) throws IOException, ServletException
	{
		PrintWriter out = getResponse().getWriter();
		getResponse().setHeader("Content-Type", "text/html");
		HashMap<String, Object> result = new HashMap<>();
		
		if (fileInfo == null)
		{
			result.put("name", null);
			result.put("originalName", null);
			result.put("size", 0);
			result.put("type", null);
			result.put("url", null);
		}
		else 
		{
			result.put("name", fileInfo.getName());
			result.put("originalName", fileInfo.getOriginalName());
			result.put("size", fileInfo.getSize());
			result.put("type", fileInfo.getType());
			result.put("url", fileInfo.getUrl());
		}
		
		result.put("state", state);
		out.write(JsonUtil.stringify(result));
		out.flush();
		out.close();
	}
	
	/**
	 * JSON形式返回UMEditor所需要的数据，状态为SUCCESS
	 * @param fileInfo
	 * @throws IOException
	 * @throws ServletException
	 */
	protected void renderUMSuccess(UploadFileInfo fileInfo) throws IOException, ServletException
	{
		renderUMResult(fileInfo, "SUCCESS");
	}
	
	/**
	 * JSON形式返回UMEditor所需要的数据，状态为ERROR
	 * @param fileInfo
	 * @throws IOException
	 * @throws ServletException
	 */
	protected void renderUMError() throws IOException, ServletException
	{
		renderUMResult(null, "ERROR");
	}
	
	// ==错误消息传递====================================================================

	/**
	 * 设置错误信息,跳转到固定错误页面
	 */
	protected void renderToErrorPage(String errMsg)
	{
		renderToErrorPage(errMsg, errorPage);
	}
	
	//统一错误页面
	private String errorPage = "error.jsp";
	
	protected synchronized void setErrorPage(String path)
	{
		this.errorPage = path;
	}
	
	protected String getErrorPage()
	{
		return this.errorPage;
	}
	
	/**
	 * 设置错误信息，跳转到指定页面
	 */
	protected void renderToErrorPage(String errMsg, String pagePath)
	{
		setAttr("errMsg", errMsg);
		forward(pagePath);
	}

	/**
	 * 设置错误信息，跳转到原来请求的页面
	 */
	protected void renderErrorToOriginPage(String errMsg)
	{
		// 这里可能会因为端口的问题而出错。因为我一直都是用80的端口，所以就没有额外的处理了
		HttpServletRequest request = getRequest();
		//hack
		renderToErrorPage(errMsg, request.getHeader("Referer")
									.replaceFirst(request.getHeader("Origin"), "")
									.replaceFirst(request.getContextPath(), ""));
	}
	
	
	/**
	 * 设置参数，跳转到指定页面
	 */
	protected void renderPage(String pagePath)
	{
		forward(pagePath);
	}
	
	// ==about pagination===============================================

	/**
	 * 设置分页参数
	 */
	protected void setPaginateInfo(String targetPath, long currentPage, long totalPages,
										long countPerPage, int countCurPage)
	{
		if (targetPath.trim().startsWith("/"))
			targetPath = targetPath.trim().substring(1);
			
		// 如果是带额外参数来分页的话，那么在表单提交的地方应当以另外的方式提交该参数
		String[] target = null;
		if (targetPath.contains("?")) 
			target = targetPath.split("\\?");
		
		String[] extraParams = new String[0];
		if (target != null && target.length >= 1) 
			extraParams = target[1].split("&");

		String linkPath = target != null ? target[0] : targetPath;
		for (int index = 0; index < extraParams.length; index++)
		{
			try
			{
				// 编码URL中的参数，可能会出现一些特殊字符，另外只是在QueryString中需要
				// 所以在post的表单中还是需要把数据重新解码
				// URLEncoder/URLDecoder对应的应该是JS中的encodeURLComponent/decodeURLComponent
				// 每种编码方式，有些字符是不会被编码的，而可能真正的数据确实需要
				int equalPos = extraParams[index].indexOf("=");
				extraParams[index] = extraParams[index].substring(0, equalPos+1) + 
									URLEncoder.encode(extraParams[index].substring(equalPos+1), "UTF-8")
									// 在这种编码方式，空格会被替换成'+'
									.replaceAll("\\+", "%20");
				linkPath += ((index == 0) ? "?" : "") + extraParams[index];
			}
			catch (UnsupportedEncodingException e)
			{
			}
		}
		
		setAttr("linkPath", linkPath);
		setAttr("targetPost", target != null ? target[0] : targetPath);
		setAttr("extraParams", extraParams);
		setAttr("currentPage", currentPage);
		setAttr("totalPages", totalPages);
		setAttr("countPerPage", countPerPage);
		setAttr("countCurPage", countCurPage);
	}
	
	
	protected void setPaginateInfo(String targetPath, Page page)
	{
		if (page != null)
			setPaginateInfo(targetPath, page.getCurrentPage(), page.getTotalPages(),
							page.getCountPerPage(), page.getCountCurPage());
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////
	//getter/setter
	////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * 获取当前的权限检查器 
	 */
	public IRuleChecker getPermissionChecker()
	{
		return this.permissionChecker;
	}
	
	/**
	 * 获取当前的基本视图路径 
	 */
	public String getBaseViewPath()
	{
		return baseViewPath;
	}

	
	public boolean isCopeWithJSONByDefault()
	{
		return copeWithJSONByDefault;
	}

	/**
	 * 设置在BaseHttpServlet中出错是否以JSON形式传递错误
	 */
	protected synchronized void setCopeWithJSONByDefault(boolean copeWithJSONByDefault)
	{
		this.copeWithJSONByDefault = copeWithJSONByDefault;
	}

	public String getUploadFilePath()
	{
		return uploadFilePath;
	}

	/**
	 * 这里指示的路径必须存在，不然会报错。（这里就不依赖程序去创建了）
	 * slash统一用的是正斜杠"/"
	 */
	protected synchronized void setUploadFilePath(String uploadFilePath)
	{
		if (uploadFilePath == null)
			return;

		if (!uploadFilePath.endsWith("/"))
			this.uploadFilePath = uploadFilePath + "/";

		this.uploadFilePath = uploadFilePath;
	}
}
